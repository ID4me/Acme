/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.controller;

import de.denic.acme.server.common.AcmeMediaTypes;
import de.denic.acme.server.model.Account;
import de.denic.acme.server.model.AcmeRequest.CompactJwsSerializationToJwtContextConverter;
import de.denic.acme.server.model.AcmeRequest.Context;
import de.denic.acme.server.model.AcmeRequest.FlattenedJwsJsonSerializationToJwtContextConverter;
import de.denic.acme.server.model.NewAccountRequest;
import de.denic.acme.server.model.NewAccountRequest.Payload;
import de.denic.acme.server.problem.ProblemDetail;
import de.denic.acme.server.repo.AccountDAO;
import de.denic.acme.server.repo.DuplicateAccountPublicKeyException;
import de.denic.acme.server.repo.NonceDAO;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.security.PublicKey;
import java.util.Collection;
import java.util.Optional;

import static de.denic.acme.server.model.Account.Status.valid;
import static de.denic.acme.server.problem.AcmeProblemURIs.ACCOUNT_DOES_NOT_EXIST;
import static de.denic.acme.server.problem.ProblemDetail.APPLICATION_PROBLEM_JSON;
import static java.lang.Boolean.FALSE;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequestUri;

@Immutable
@Controller
@RequestMapping("/newAccount")
public class NewAccountController implements AcmeMediaTypes, ControllerUriBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(NewAccountController.class);
  private static final URI NO_INSTANCE = null;
  private static final String NO_DETAIL = null;

  private final NewAccountRequest compactJwsNewAccountRequest;
  private final NewAccountRequest flattenedJwsJsonNewAccountRequest;
  private final AccountDAO accountDAO;

  /**
   * @param nonceDAO   Required
   * @param accountDAO Required
   */
  public NewAccountController(final NonceDAO nonceDAO, final AccountDAO accountDAO) {
    Validate.notNull(accountDAO, "Missing account DAO component");
    this.accountDAO = accountDAO;
    this.compactJwsNewAccountRequest = new NewAccountRequest
            (accountDAO, nonceDAO, CompactJwsSerializationToJwtContextConverter.INSTANCE);
    this.flattenedJwsJsonNewAccountRequest = new NewAccountRequest
            (accountDAO, nonceDAO, FlattenedJwsJsonSerializationToJwtContextConverter.INSTANCE);
  }

  @RequestMapping(method = POST, consumes = APPLICATION_JOSE_JSON_VALUE)
  public ResponseEntity<?> consumingPostedJoseJson(@RequestBody final String requestBody) {
    return consumeWithConverter(requestBody, flattenedJwsJsonNewAccountRequest);
  }

  @RequestMapping(method = POST, consumes = APPLICATION_JWS_VALUE)
  public ResponseEntity<?> consumingPostedJws(@RequestBody final String requestBody) {
    return consumeWithConverter(requestBody, compactJwsNewAccountRequest);
  }

  private ResponseEntity<?> consumeWithConverter(final String requestBody, final NewAccountRequest newAccountRequest) {
    LOG.info("New ACCOUNT requested");
    final Context<Payload> context = newAccountRequest.convert(requestBody);
    final PublicKey publicKey = context.getKeyToVerifyJws();
    final Payload payload = context.getPayload();
    final HttpHeaders additionalHeaders = new HttpHeaders();
    additionalHeaders.set("Link", "<" + directoryResourceUriBuilderFrom(fromCurrentRequestUri()).build()
            .normalize() + ">;rel=\"index\"");
    if (defaultIfNull(payload.getOnlyReturnExisting(), FALSE)) {
      return returnExistingAccount(publicKey, additionalHeaders);
    }

    final UriComponentsBuilder accountBaseUriBuilder = accountResourceUriBuilderFrom(fromCurrentRequestUri());
    try {
      final Account account = createAndStoreAccountApplying(payload.getContacts(), publicKey);
      final URI location = locationUriOf(account, accountBaseUriBuilder);
      return ResponseEntity.created(location).headers(additionalHeaders).body(account);
    } catch (DuplicateAccountPublicKeyException e) {
      additionalHeaders.setLocation(locationUriOf(e.getIdOfKnownAccount(), accountBaseUriBuilder));
      return ResponseEntity.ok().headers(additionalHeaders).build();
    }
  }

  private ResponseEntity<?> returnExistingAccount(PublicKey publicKey, HttpHeaders additionalHeaders) {
    final Optional<Account> optKnownAccount = accountDAO.findWith(publicKey);
    if (!optKnownAccount.isPresent()) {
      final ProblemDetail problem = new ProblemDetail(ACCOUNT_DOES_NOT_EXIST, "Account does not exist.",
              BAD_REQUEST, NO_DETAIL, NO_INSTANCE);
      additionalHeaders.setContentType(APPLICATION_PROBLEM_JSON);
      return badRequest().headers(additionalHeaders).body(problem);
    }

    final Account knownAccount = optKnownAccount.get();
    additionalHeaders.set("Location", locationUriOf(knownAccount, fromCurrentRequestUri()).toASCIIString());
    return ok().headers(additionalHeaders).body(knownAccount);
  }

  private URI locationUriOf(final Account account, final UriComponentsBuilder uriBuilder) {
    return locationUriOf(account.getId(), uriBuilder);
  }

  private URI locationUriOf(final Account.Id accountId, final UriComponentsBuilder uriBuilder) {
    return accountResourceUriBuilderFrom(uriBuilder.path("..")).path("/").path(Integer.toString(accountId
            .getValue())).build().normalize().toUri();
  }

  private Account createAndStoreAccountApplying(final Collection<String> contacts, final PublicKey publicKey) throws DuplicateAccountPublicKeyException {
    return accountDAO.createWith(valid, publicKey, contacts);
  }

}
