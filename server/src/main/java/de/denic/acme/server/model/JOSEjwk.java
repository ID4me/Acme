/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.model;

import de.denic.acme.server.model.AcmeRequest.Context;
import de.denic.acme.server.model.AcmeRequest.JoseHeaderPredicate;
import org.jose4j.jwk.PublicJsonWebKey;
import org.jose4j.jwt.consumer.JwtContext;
import org.jose4j.lang.JoseException;

import javax.annotation.concurrent.Immutable;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Immutable
public final class JOSEjwk implements JoseHeaderPredicate {

  public static final JOSEjwk INSTANCE = new JOSEjwk();
  public static final String JWK = "jwk";

  private static final Class<PublicJsonWebKey> TYPE = PublicJsonWebKey.class;
  private static final String NO_EXPLICIT_JCA_PROVIDER = null;
  private static final PublicJsonWebKey ERRONEOUS_JWK_HEADER = null;

  private final boolean appendErrorMessages;

  public JOSEjwk(final boolean appendErrorMessages) {
    this.appendErrorMessages = appendErrorMessages;
  }

  public JOSEjwk() {
    this(true);
  }

  @Override
  public boolean test(final JwtContext inputContext, final Context<?> resultContext) {
    final List<PublicJsonWebKey> jwks = inputContext.getJoseObjects().stream().map(jws -> {
      try {
        return jws.getHeaders().getPublicJwkHeaderValue(JWK, NO_EXPLICIT_JCA_PROVIDER);
      } catch (JoseException e) {
        resultContext.appendErrorMessage("Invalid JOSE header '" + JWK + "': " + e.getMessage() + ".");
        return ERRONEOUS_JWK_HEADER;
      } catch (NullPointerException e) {
        if (appendErrorMessages) {
          resultContext.appendErrorMessage("JOSE header '" + JWK + "' is missing.");
        }
        return ERRONEOUS_JWK_HEADER;
      }
    }).collect(toList());
    if (jwks.isEmpty()) {
      if (appendErrorMessages) {
        resultContext.appendErrorMessage("JOSE header '" + JWK + "' is missing.");
      }
      return false;
    }

    final PublicJsonWebKey jsonWebKey = jwks.get(0);
    if (jsonWebKey != ERRONEOUS_JWK_HEADER) {
      resultContext.setJsonWebKey(jsonWebKey.getPublicKey());
    }
    return jsonWebKey != ERRONEOUS_JWK_HEADER;
  }

}
