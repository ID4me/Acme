/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.acme.server.common.AcmeMediaTypes;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.concurrent.Immutable;
import java.net.URI;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequestUri;

@Immutable
@Controller
@RequestMapping("/directory")
public class DirectoryController implements AcmeMediaTypes {

  @RequestMapping(method = {GET, POST}, produces = APPLICATION_JSON_VALUE)
  public @ResponseBody
  Directory getDirectory() {
    return new Directory(fromCurrentRequestUri());
  }

  /**
   * <a href="https://tools.ietf.org/html/draft-ietf-acme-acme-09#section-7.1.1">ACME Internet Draft 9, chapter
   * 7.1.1</a>
   */
  @Immutable
  public static final class Directory implements ControllerUriBuilder {

    private final URI newAuthorizationUri, newNonceUri, newAccountUri;

    /**
     * @param uriComponentsBuilder Required
     */
    private Directory(final ServletUriComponentsBuilder uriComponentsBuilder) {
      Validate.notNull(uriComponentsBuilder, "Missing URI component builder");
      this.newAuthorizationUri = newAuthorizationResourceUriBuilderFrom(uriComponentsBuilder.cloneBuilder()).build().normalize().toUri();
      this.newNonceUri = newNonceResourceUriBuilderFrom(uriComponentsBuilder.cloneBuilder()).build().normalize().toUri();
      this.newAccountUri = newAccountResourceUriBuilderFrom(uriComponentsBuilder.cloneBuilder()).build().normalize()
              .toUri();
    }

    /**
     * @return Never <code>null</code>
     */
    @JsonProperty("newNonce")
    public URI getNewNonce() {
      return newNonceUri;
    }

    /**
     * @return Never <code>null</code>
     */
    @JsonProperty("newAccount")
    public URI getNewAccount() {
      return newAccountUri;
    }

    /**
     * @return Never <code>null</code>
     */
    @JsonProperty("newAuthz")
    public URI getNewAuthorization() {
      return newAuthorizationUri;
    }

    @Override
    public String toString() {
      return "Directory{" +
              "newAuthorizationUri=" + newAuthorizationUri +
              ", newNonceUri=" + newNonceUri +
              ", newAccountUri=" + newAccountUri +
              '}';
    }
  }
}
