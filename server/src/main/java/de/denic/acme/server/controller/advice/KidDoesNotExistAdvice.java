/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.controller.advice;

import de.denic.acme.server.controller.KidDoesNotExistException;
import de.denic.acme.server.problem.ProblemDetail;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.annotation.concurrent.Immutable;
import java.net.URI;

import static de.denic.acme.server.problem.AcmeProblemURIs.ACCOUNT_DOES_NOT_EXIST;
import static de.denic.acme.server.problem.ProblemDetail.APPLICATION_PROBLEM_JSON;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ControllerAdvice
@Immutable
public final class KidDoesNotExistAdvice {

  private static final URI NO_INSTANCE = null;

  @ExceptionHandler(value = KidDoesNotExistException.class)
  public ResponseEntity<ProblemDetail> respondTo(final KidDoesNotExistException exception) {
    final HttpStatus status = BAD_REQUEST;
    final ProblemDetail problemDetail = new ProblemDetail(ACCOUNT_DOES_NOT_EXIST, "KID addresses an unknown Account.",
            status, exception.getMessage(), NO_INSTANCE);
    return ResponseEntity.status(status).contentType(APPLICATION_PROBLEM_JSON).body(problemDetail);
  }
}
