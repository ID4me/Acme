/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

public interface ControllerUriBuilder {

  String DIRECTORY_RESOURCE_MAPPING = DirectoryController.class.getAnnotation(RequestMapping.class).value()[0];
  String NEW_AUTHORIZATION_RESOURCE_MAPPING = NewAuthorizationController.class.getAnnotation(RequestMapping.class).value()[0];
  String AUTHORIZATION_RESOURCE_MAPPING = AuthorizationController.class.getAnnotation(RequestMapping.class).value()[0];
  String ACCOUNT_RESOURCE_MAPPING = AccountController.class.getAnnotation(RequestMapping.class).value()[0];
  String NEW_NONCE_RESOURCE_MAPPING = NewNonceController.class.getAnnotation(RequestMapping.class).value()[0];
  String NEW_ACCOUNT_RESOURCE_MAPPING = NewAccountController.class.getAnnotation(RequestMapping.class).value()[0];

  default UriComponentsBuilder newAuthorizationResourceUriBuilderFrom(final UriComponentsBuilder uriComponentsBuilder) {
    return buildUrlFrom(uriComponentsBuilder, NEW_AUTHORIZATION_RESOURCE_MAPPING);
  }

  default UriComponentsBuilder authorizationResourceUriBuilderFrom(final UriComponentsBuilder uriComponentsBuilder) {
    return buildUrlFrom(uriComponentsBuilder, AUTHORIZATION_RESOURCE_MAPPING);
  }

  default UriComponentsBuilder newNonceResourceUriBuilderFrom(final UriComponentsBuilder uriComponentsBuilder) {
    return buildUrlFrom(uriComponentsBuilder, NEW_NONCE_RESOURCE_MAPPING);
  }

  default UriComponentsBuilder newAccountResourceUriBuilderFrom(final UriComponentsBuilder uriComponentsBuilder) {
    return buildUrlFrom(uriComponentsBuilder, NEW_ACCOUNT_RESOURCE_MAPPING);
  }

  default UriComponentsBuilder accountResourceUriBuilderFrom(final UriComponentsBuilder uriComponentsBuilder) {
    return buildUrlFrom(uriComponentsBuilder, ACCOUNT_RESOURCE_MAPPING);
  }

  default UriComponentsBuilder directoryResourceUriBuilderFrom(final UriComponentsBuilder uriComponentsBuilder) {
    return buildUrlFrom(uriComponentsBuilder, DIRECTORY_RESOURCE_MAPPING);
  }

  default UriComponentsBuilder buildUrlFrom(final UriComponentsBuilder uriComponentsBuilder, final String path) {
    return uriComponentsBuilder.path("/..").path(path);
  }

}
