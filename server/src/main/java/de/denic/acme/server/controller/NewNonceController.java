/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.controller;

import de.denic.acme.server.repo.NonceDAO;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.concurrent.Immutable;

import static de.denic.acme.server.model.Nonce.REPLAY_NONCE_HEADER;
import static org.springframework.http.CacheControl.noStore;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@Immutable
@Controller
@RequestMapping("/newNonce")
/**
 * Produces <a href="https://de.wikipedia.org/wiki/Nonce">Nonces</a> containing 16 random bytes followed by 8 bytes from epoch seconds.
 */
public class NewNonceController {

  private static final Logger LOG = LoggerFactory.getLogger(NewNonceController.class);

  private final NonceDAO nonceDAO;

  /**
   * @param nonceDAO Required
   */
  public NewNonceController(final NonceDAO nonceDAO) {
    Validate.notNull(nonceDAO, "Missing nonce DAO");
    this.nonceDAO = nonceDAO;
  }

  @RequestMapping(method = {GET, POST, HEAD})
  public ResponseEntity<String> viaGet() {
    LOG.info("New NONCE requested");
    return ResponseEntity.noContent().header(REPLAY_NONCE_HEADER, nonceDAO.createRemembered().getValue()).cacheControl(noStore()).build();
  }

}
