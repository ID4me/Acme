/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.model;

import de.denic.acme.server.model.AcmeRequest.Context;
import de.denic.acme.server.model.AcmeRequest.JoseHeader;
import org.jose4j.jwt.consumer.JwtContext;

import javax.annotation.concurrent.Immutable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

@Immutable
public final class JOSEkid implements JoseHeader<URL> {

  public static final JOSEkid INSTANCE = new JOSEkid();
  public static final String KID = "kid";

  private static final Class<URL> TYPE = URL.class;

  private final boolean appendErrorMessage;

  public JOSEkid(final boolean appendErrorMessage) {
    this.appendErrorMessage = appendErrorMessage;
  }

  public JOSEkid() {
    this(true);
  }

  @Override
  public boolean test(final JwtContext inputContext, final Context<?> resultContext) {
    final Optional<String> optKid = inputContext.getJoseObjects().stream().map(jws -> jws.getHeaders()
            .getStringHeaderValue(KID)).filter(jws -> jws != null).findFirst();
    if (!optKid.isPresent()) {
      if (appendErrorMessage) {
        resultContext.appendErrorMessage("Missing JOSE header '" + KID + "'.");
      }
      return false;
    }

    final String kid = optKid.get();
    try {
      resultContext.put(this, new URL(kid));
    } catch (MalformedURLException e) {
      if (appendErrorMessage) {
        resultContext.appendErrorMessage("Invalid JOSE header '" + KID + "' ('" + kid + "'): " + e.getMessage() + ".");
      }
      return false;
    }

    return true;
  }

  @Override
  public String contextKey() {
    return KID;
  }

  @Override
  public Class<URL> typeOfContextValue() {
    return TYPE;
  }

}
