/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.repo.impl;

import de.denic.acme.server.model.Account;
import de.denic.acme.server.model.Account.Id;
import de.denic.acme.server.model.Account.Status;
import de.denic.acme.server.repo.AccountDAO;
import de.denic.acme.server.repo.DuplicateAccountPublicKeyException;
import io.micrometer.core.instrument.Metrics;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.ThreadSafe;
import java.net.URL;
import java.security.PublicKey;
import java.util.*;

import static java.util.Arrays.asList;
import static java.util.Optional.*;

@Component
@ThreadSafe
public final class AccountDAOInMemoryImpl implements AccountDAO {

  private static final Logger LOG = LoggerFactory.getLogger(AccountDAOInMemoryImpl.class);

  private final Object guardOfInternalMaps = new Object();
  @GuardedBy("guardOfInternalMaps")
  private final Map<Id, Account> idToAccountMapping = new HashMap<>();
  @GuardedBy("guardOfInternalMaps")
  private final Map<PublicKey, Id> publicKeyToIdMapping = new HashMap<>();

  public AccountDAOInMemoryImpl() {
    Metrics.gauge(getClass().getSimpleName() + ".savedAccounts", this, AccountDAOInMemoryImpl::amountOfSavedAccounts);
  }

  @Override
  public Account createWith(final Status status, final PublicKey publicKey, final Collection<String> contacts) throws
          DuplicateAccountPublicKeyException {
    LOG.info("Creating account with: {}, {}, {}", status, publicKey, contacts);
    final Account result;
    synchronized (guardOfInternalMaps) {
      final Id idOfAccount = publicKeyToIdMapping.get(publicKey);
      if (idOfAccount != null) {
        throw new DuplicateAccountPublicKeyException(publicKey, idOfAccount);
      }

      final Optional<Id> currentMaxId = idToAccountMapping.keySet().stream().max((id1, id2) -> id1.compareTo(id2));
      final Id newAccountsId = new Id(currentMaxId.map(id -> id.getValue() + 1).orElse(0));
      result = new Account(newAccountsId, status, publicKey, contacts);
      idToAccountMapping.put(newAccountsId, result);
      publicKeyToIdMapping.put(publicKey, newAccountsId);
    }
    return result;
  }

  @Override
  public Optional<Account> updateWith(final Id accountId, final Collection<String> contacts) {
    Validate.notNull(accountId, "Missing account ID");
    LOG.info("Updating account '{}' with contacts: {}", accountId, contacts);
    final Account updatedAccount;
    synchronized (guardOfInternalMaps) {
      final Account knownAccount = idToAccountMapping.get(accountId);
      if (knownAccount == null) {
        return empty();
      }

      updatedAccount = new Account(knownAccount.getId(), knownAccount.getStatus(), knownAccount.getPublicKey(), contacts);
      idToAccountMapping.put(updatedAccount.getId(), updatedAccount);
      publicKeyToIdMapping.put(updatedAccount.getPublicKey(), updatedAccount.getId());
    }
    return of(updatedAccount);
  }

  @Override
  public Optional<PublicKey> getPublicKeyWith(final URL accountURL) {
    Validate.notNull(accountURL, "Missing account URL");
    LOG.info("Requested: Public Key of account URL {}", accountURL);
    final List<String> pathElements = asList(accountURL.getPath().split("/"));
    final Id accountId = new Id(Integer.parseInt(pathElements.get(pathElements.size() - 1)));
    return ofNullable(getAccountBy(accountId)).map(Account::getPublicKey);
  }

  @Override
  public Optional<Account> findWith(final PublicKey publicKey) {
    Validate.notNull(publicKey, "Missing public key");
    LOG.info("Searching account with public key {}", publicKey);
    synchronized (guardOfInternalMaps) {
      final Id idOfAccount = publicKeyToIdMapping.get(publicKey);
      if (idOfAccount == null) return empty();

      return ofNullable(idToAccountMapping.get(idOfAccount));
    }
  }

  @Override
  public Optional<Account> findBy(final Id accountId) {
    Validate.notNull(accountId, "Missing Account ID");
    LOG.info("Requested: Account of ID {}", accountId);
    return ofNullable(getAccountBy(accountId));
  }

  /**
   * @param accountId Required
   * @return Optional
   */
  private Account getAccountBy(final Id accountId) {
    synchronized (guardOfInternalMaps) {
      return idToAccountMapping.get(accountId);
    }
  }

  @Override
  public void clearAll() {
    synchronized (guardOfInternalMaps) {
      idToAccountMapping.clear();
      publicKeyToIdMapping.clear();
    }
  }

  private long amountOfSavedAccounts() {
    synchronized (guardOfInternalMaps) {
      return idToAccountMapping.size();
    }
  }

}
