/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.controller;

import de.denic.acme.server.common.AcmeMediaTypes;
import de.denic.acme.server.model.Account;
import de.denic.acme.server.model.AccountRequest;
import de.denic.acme.server.model.AcmeRequest;
import de.denic.acme.server.problem.AcmeProblemURIs;
import de.denic.acme.server.problem.ProblemDetail;
import de.denic.acme.server.repo.AccountDAO;
import de.denic.acme.server.repo.NonceDAO;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.util.Optional;

import static de.denic.acme.server.problem.ProblemDetail.APPLICATION_PROBLEM_JSON;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequestUri;

@Immutable
@Controller
@RequestMapping("/account")
public class AccountController implements AcmeMediaTypes, ControllerUriBuilder, AcmeProblemURIs {

  private static final Logger LOG = LoggerFactory.getLogger(AccountController.class);
  private static final URI NO_INSTANCE = null;

  private final AccountRequest compactJwsAccountRequest;
  private final AccountRequest flattenedJwsJsonAccountRequest;
  private final AccountDAO accountDAO;

  /**
   * @param accountDAO Required
   * @param nonceDAO   Required
   */
  public AccountController(final AccountDAO accountDAO, final NonceDAO nonceDAO) {
    Validate.notNull(accountDAO, "Missing Account DAO component");
    this.compactJwsAccountRequest = new AccountRequest
            (accountDAO, nonceDAO, AcmeRequest.CompactJwsSerializationToJwtContextConverter.INSTANCE);
    this.flattenedJwsJsonAccountRequest = new AccountRequest
            (accountDAO, nonceDAO, AcmeRequest.FlattenedJwsJsonSerializationToJwtContextConverter.INSTANCE);
    this.accountDAO = accountDAO;
  }

  /**
   * According to <a href="https://tools.ietf.org/html/draft-ietf-acme-acme-09#section-7.3.3">chapter 7.3.3 of ACME Draft 9</a> GET requests on Account resource is NOT allowed!
   */
  public ResponseEntity<Account> getAccount(@PathVariable final Account.Id accountId) {
    LOG.info("Received request for '{}'", accountId);
    final HttpHeaders additionalHeaders = new HttpHeaders();
    additionalHeaders.set("Link", "<" + directoryResourceUriBuilderFrom(fromCurrentRequestUri().path("/..")).build()
            .normalize() + ">;rel=\"index\"");
    return accountDAO.findBy(accountId).map(account -> ok().headers(additionalHeaders).body(account)).orElseThrow(() -> new AccountDoesNotExistException
            (fromCurrentRequestUri().build().toUri()));
  }

  @RequestMapping(value = "/{accountId}", method = POST, consumes = APPLICATION_JOSE_JSON_VALUE)
  public ResponseEntity<?> consumingPostedJoseJson(@PathVariable final Account.Id accountId, @RequestBody final String requestBody) {
    return updateAccount(accountId, requestBody, flattenedJwsJsonAccountRequest);
  }

  @RequestMapping(value = "/{accountId}", method = POST, consumes = APPLICATION_JWS_VALUE)
  public ResponseEntity<?> consumingPostedJws(@PathVariable final Account.Id accountId, @RequestBody final String requestBody) {
    return updateAccount(accountId, requestBody, compactJwsAccountRequest);
  }

  private ResponseEntity<?> updateAccount(final Account.Id accountId, final String requestBody, final AccountRequest
          accountRequest) {
    LOG.info("Updating ACCOUNT '{}': {}", accountId, requestBody);
    final AccountRequest.Payload payload = accountRequest.convert(requestBody).getPayload();
    final Optional<Account> optUpdatedAccount = accountDAO.updateWith(accountId, payload.getContacts());
    if (optUpdatedAccount.isPresent()) {
      return ok(optUpdatedAccount.get());
    }

    final HttpHeaders additionalHeaders = new HttpHeaders();
    additionalHeaders.setContentType(APPLICATION_PROBLEM_JSON);
    return status(NOT_FOUND).headers(additionalHeaders).body(new ProblemDetail(ACCOUNT_DOES_NOT_EXIST, "Account is " +
            "unknown", NOT_FOUND,
            "Account ID '" + accountId + "'", NO_INSTANCE));
  }

}
