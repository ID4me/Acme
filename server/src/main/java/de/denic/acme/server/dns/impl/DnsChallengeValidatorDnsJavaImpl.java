/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.dns.impl;

import de.denic.acme.server.dns.DnsChallengeValidator;
import de.denic.acme.server.model.Dns01Challenge;
import de.denic.acme.server.model.Identifier;
import de.denic.acme.server.problem.ProblemDetail;
import org.apache.commons.lang3.Validate;
import org.jose4j.base64url.Base64Url;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.xbill.DNS.*;

import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static de.denic.acme.server.problem.AcmeProblemURIs.INCORRECT_RESPONSE;
import static java.nio.charset.StandardCharsets.US_ASCII;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.join;
import static org.springframework.http.HttpStatus.OK;
import static org.xbill.DNS.Name.*;
import static org.xbill.DNS.Type.TXT;
import static org.xbill.DNS.Type.string;

@Component
@Immutable
public final class DnsChallengeValidatorDnsJavaImpl implements DnsChallengeValidator {

  private static final Logger LOG = LoggerFactory.getLogger(DnsChallengeValidatorDnsJavaImpl.class);
  private static final String NO_DETAIL = null;
  private static final URI NO_INSTANCE = null;
  private static final Name ACME_CHALLENGE_PREFIX = fromConstantString("_acme-challenge");
  private static final MessageDigest SHA_256_DIGEST;

  static {
    try {
      SHA_256_DIGEST = MessageDigest.getInstance("SHA-256");
    } catch (NoSuchAlgorithmException e) {
      throw new ExceptionInInitializerError(e);
    }
  }

  private final Resolver dnsResolver;
  private final String dnsResolverDescription;

  /**
   * @param dnsResolver Optional, if missing a default {@link SimpleResolver} gets used applying OS stub resolver.
   */
  public DnsChallengeValidatorDnsJavaImpl(final Resolver dnsResolver) throws UnknownHostException {
    if (dnsResolver == null) {
      final SimpleResolver defaultResolver = new SimpleResolver();
      this.dnsResolver = defaultResolver;
      this.dnsResolverDescription = defaultResolver.getAddress().toString();
      LOG.info("Applying default DNS resolver {}", defaultResolver.getAddress());
    } else {
      this.dnsResolver = dnsResolver;
      this.dnsResolverDescription = (this.dnsResolver instanceof SimpleResolver ? ((SimpleResolver) this.dnsResolver)
              .getAddress().toString() : "UNKNOWN");
      LOG.info("Applying DNS resolver {}", dnsResolverDescription);
    }
  }

  /**
   * @see #DnsChallengeValidatorDnsJavaImpl(Resolver)
   */
  public DnsChallengeValidatorDnsJavaImpl() throws UnknownHostException {
    this(null);
  }

  @Override
  public Dns01Challenge validate(final Identifier identifier, final Dns01Challenge challenge, final String keyAuthorization) {
    Validate.notNull(identifier, "Missing identifier");
    Validate.notNull(challenge, "Missing challenge");
    Validate.notEmpty(keyAuthorization, "Missing key authorization");
    final Name nameToLookup;
    try {
      final Name nameOfIdentifier = fromString(identifier.getValue(), root);
      nameToLookup = concatenate(ACME_CHALLENGE_PREFIX, nameOfIdentifier);
    } catch (TextParseException | NameTooLongException e) {
      throw new RuntimeException(identifier + " invalid", e);
    }

    final Lookup lookupOperation = new Lookup(nameToLookup, TXT);
    lookupOperation.setResolver(dnsResolver);
    lookupOperation.setCache(null);
    LOG.info("DNS lookup: {} records of '{}' (via resolver '{}')", string(TXT), nameToLookup, dnsResolverDescription);
    final Instant startedAt = Instant.now();
    final org.xbill.DNS.Record[] lookupResult = lookupOperation.run();
    final Duration lookupDuration = Duration.between(startedAt, Instant.now());
    LOG.info("DNS lookup yields: {} (took {})", Arrays.toString(lookupResult), lookupDuration);
    final byte[] sha256DigestOfKeyAuthorization = SHA_256_DIGEST.digest(keyAuthorization.getBytes(US_ASCII));
    final String base64OfSha256OfKeyAuthorization = Base64Url.encode(sha256DigestOfKeyAuthorization);
    final Collection<String> base64OfSha256OfKeyAuthorizationsFromDNS = extractTokenFrom(lookupResult);
    if (base64OfSha256OfKeyAuthorizationsFromDNS.isEmpty()) {
      LOG.info("Found no DNS entry solving '{}'", challenge);
      challenge.addError(new ProblemDetail(INCORRECT_RESPONSE, "Found no DNS entry solving challenge", OK,
              NO_DETAIL, NO_INSTANCE));
    } else {
      final boolean matchingDnsEntryFound = base64OfSha256OfKeyAuthorizationsFromDNS.stream().anyMatch(base64OfSha256OfKeyAuthorization::equals);
      if (matchingDnsEntryFound) {
        challenge.setNotPending();
      } else {
        LOG.info("Found no Base64-encoded SHA256-hash matching keyAuthorization '{}' in DNS response: {}",
                base64OfSha256OfKeyAuthorization, base64OfSha256OfKeyAuthorizationsFromDNS);
        challenge.addError(new ProblemDetail(INCORRECT_RESPONSE, "Challenge has not been solved by your response", OK,
                "TXT-Lookup of '" + nameToLookup + "' reveals " + base64OfSha256OfKeyAuthorizationsFromDNS + ", but received '" +
                        base64OfSha256OfKeyAuthorization + "'", NO_INSTANCE));
      }
    }
    return challenge;
  }

  /**
   * @param lookupResult Optional
   * @return Never <code>null</code>
   */
  private @NotNull List<String> extractTokenFrom(final org.xbill.DNS.Record[] lookupResult) {
    if (lookupResult == null) {
      return emptyList();
    }

    return stream(lookupResult)
            .map(TXTRecord.class::cast)
            .map(txtRecord -> join(txtRecord.getStrings(), ""))
            .collect(toList());
  }

  @Override
  public String toString() {
    return "DnsChallengeValidatorDnsJavaImpl{" +
            "dnsResolver=" + dnsResolverDescription + '\'' +
            '}';
  }

}
