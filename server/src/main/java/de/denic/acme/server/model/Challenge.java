/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.acme.server.problem.ProblemDetail;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.NotThreadSafe;
import javax.annotation.concurrent.ThreadSafe;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static de.denic.acme.server.model.Challenge.Status.invalid;
import static de.denic.acme.server.model.Challenge.Status.valid;
import static java.util.Locale.ENGLISH;

@ThreadSafe
public abstract class Challenge<SUBTYPE extends Challenge<SUBTYPE>> {

  private final Class<SUBTYPE> subtypeClass;
  private final String type;
  private final URL url;
  @GuardedBy("this")
  private Status status;
  @GuardedBy("this")
  private List<ProblemDetail> errors = new LinkedList<>();

  /**
   * @param type         Required to be not empty, will be converted to lowercase.
   * @param status       Required
   * @param subtypeClass Required
   * @param url          Required
   */
  protected Challenge(final Class<SUBTYPE> subtypeClass, final String type, final Status status, final URL url) {
    Validate.notEmpty(type, "Missing type");
    Validate.notNull(status, "Missing optStatus");
    Validate.notNull(subtypeClass, "Missing subtype class");
    Validate.notNull(url, "Missing URL");
    this.type = type.toLowerCase(ENGLISH);
    this.status = status;
    this.subtypeClass = subtypeClass;
    this.url = url;
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonProperty("type")
  public final String getType() {
    return type;
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonIgnore
  public final URL getUrl() {
    return url;
  }

  /**
   * @return Optional
   */
  @JsonProperty("url")
  public final String getUrlValue() {
    return url.toExternalForm();
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonProperty("status")
  public synchronized final Status getStatus() {
    return status;
  }

  /**
   * @return This instance (e.g. for mapping)
   */
  public final SUBTYPE setNotPending() {
    synchronized (this) {
      this.status = (errors.isEmpty() ? valid : invalid);
    }
    return subtypeClass.cast(this);
  }

  @JsonIgnore
  public final boolean isValid() {
    return valid.equals(getStatus());
  }

  /**
   * @param problemDetail Optional, no-op if missing.
   * @return <code>this</code> instance.
   */
  public final SUBTYPE addError(final ProblemDetail problemDetail) {
    if (problemDetail != null) {
      synchronized (this) {
        errors.add(problemDetail);
        status = invalid;
      }
    }
    return subtypeClass.cast(this);
  }

  /**
   * @return Never <code>null</code>.
   */
  @JsonProperty("errors")
  @JsonInclude(NON_EMPTY)
  public synchronized final List<ProblemDetail> getErrors() {
    return new ArrayList<>(errors);
  }

  @Override
  public String toString() {
    return "type='" + type + '\'' +
            ", url=" + url +
            ", status='" + getStatus() +
            ", errors='" + getErrors() +
            '\'';
  }

  public enum Status {

    pending, valid, invalid

  }

  @NotThreadSafe
  public static abstract class Builder<SUBTYPE extends Challenge<SUBTYPE>> {

    protected final Status status;

    /**
     * @param status Required
     */
    protected Builder(final Status status) {
      Validate.notNull(status, "Missing status");
      this.status = status;
    }

    /**
     * @param url Required
     * @return Never <code>null</code>
     */
    public final SUBTYPE with(final URL url) {
      Validate.notNull(url, "Missing URL");
      return withNotNull(url);
    }

    /**
     * @param uri Required
     * @return Never <code>null</code>
     */
    public final SUBTYPE with(final URI uri) {
      Validate.notNull(uri, "Missing URI");
      try {
        return with(uri.normalize().toURL());
      } catch (MalformedURLException e) {
        throw new RuntimeException("Internal error", e);
      }
    }

    /**
     * @param url Required
     * @return Never <code>null</code>
     */
    protected abstract SUBTYPE withNotNull(URL url);

  }

}
