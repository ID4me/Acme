/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.model;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.denic.acme.server.common.InvalidAcmeMessageException;
import de.denic.acme.server.controller.KidDoesNotExistException;
import de.denic.acme.server.repo.AccountDAO;
import de.denic.acme.server.repo.NonceDAO;
import org.apache.commons.lang3.Validate;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.jwt.consumer.JwtContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;

import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.PublicKey;
import java.util.*;
import java.util.function.BiPredicate;

import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;
import static java.util.Collections.unmodifiableSet;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.join;
import static org.jose4j.base64url.Base64Url.encodeUtf8ByteRepresentation;
import static org.springframework.util.CollectionUtils.isEmpty;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequestUri;

/**
 * ACME requests use <a href="https://tools.ietf.org/html/rfc7515">JSON Web Signature</a> as specified by <a href="https://tools.ietf.org/html/draft-ietf-acme-acme-08#section-6">Chapter 6 of ACME draft</a>.
 */
@Immutable
public abstract class AcmeRequest<PAYLOAD> implements Converter<String, AcmeRequest.Context<PAYLOAD>> {

  private static final Logger LOG = LoggerFactory.getLogger(AcmeRequest.class);

  private final List<JoseHeaderPredicate> joseHeaderPredicates;
  private final Converter<String, JwtContext> inputToJwtContextConverter;
  private final AccountDAO accountDAO;

  /**
   * @param accountDAO                 Required
   * @param nonceDAO                   Required
   * @param inputToJwtContextConverter Required
   * @param joseHeaderPredicates       Optional
   */
  public AcmeRequest(final AccountDAO accountDAO, final NonceDAO nonceDAO, final Converter<String, JwtContext>
          inputToJwtContextConverter, final Collection<JoseHeaderPredicate> joseHeaderPredicates) {
    Validate.notNull(inputToJwtContextConverter, "Missing JWS converter");
    Validate.notNull(accountDAO, "Missing account DAO");
    this.inputToJwtContextConverter = inputToJwtContextConverter;
    this.accountDAO = accountDAO;
    final List<JoseHeaderPredicate> tempJoseHeaderPredicates = new ArrayList<>();
    tempJoseHeaderPredicates.add(new JOSEnonce(nonceDAO));
    tempJoseHeaderPredicates.add(JOSEurl.INSTANCE);
    if (!isEmpty(joseHeaderPredicates)) {
      tempJoseHeaderPredicates.addAll(joseHeaderPredicates);
    }
    this.joseHeaderPredicates = unmodifiableList(tempJoseHeaderPredicates);
  }

  /**
   * @see #AcmeRequest(AccountDAO, NonceDAO, Converter, JoseHeaderPredicate...)
   */
  public AcmeRequest(final AccountDAO accountDAO, final NonceDAO nonceDAO, final Converter<String, JwtContext> inputToJwtContextConverter, final JoseHeaderPredicate... joseHeaderPredicates) {
    this(accountDAO, nonceDAO, inputToJwtContextConverter, joseHeaderPredicates == null ? null : asList
            (joseHeaderPredicates));
  }

  /**
   * Applies JWS Compact Serialization input converter
   *
   * @see #AcmeRequest(AccountDAO, NonceDAO, Converter, Collection)
   */
  public AcmeRequest(final AccountDAO accountDAO, final NonceDAO nonceDAO, final Collection<JoseHeaderPredicate> joseHeaderPredicates) {
    this(accountDAO, nonceDAO, CompactJwsSerializationToJwtContextConverter.INSTANCE, joseHeaderPredicates);
  }

  /**
   * @see #AcmeRequest(AccountDAO, NonceDAO, Collection)
   */
  public AcmeRequest(final AccountDAO accountDAO, final NonceDAO nonceDAO, final JoseHeaderPredicate... joseHeaderPredicates) {
    this(accountDAO, nonceDAO, CompactJwsSerializationToJwtContextConverter.INSTANCE, joseHeaderPredicates);
  }

  @Override
  @NonNull
  public final Context<PAYLOAD> convert(final String source) throws InvalidAcmeMessageException {
    LOG.info("Converting: {}", source);
    final JwtContext jwtContext;
    try {
      jwtContext = inputToJwtContextConverter.convert(source);
    } catch (final IllegalArgumentException e) {
      throw new InvalidAcmeMessageException("Failed parsing of request by " + inputToJwtContextConverter + ". Message: " + source,
              e);
    }

    final Context<PAYLOAD> contextResult = new Context<>();
    final Optional<Boolean> optFailure = joseHeaderPredicates.stream().map(element -> element.test(jwtContext, contextResult)).filter(sucess -> !sucess)
            .findFirst();
    if (optFailure.isPresent()) {
      throw new InvalidAcmeMessageException(join(contextResult.getErrors(), ' '), source);
    }

    final Optional<URL> optJOSEkid = contextResult.has(JOSEkid.INSTANCE);
    if (optJOSEkid.isPresent()) {
      final URL accountURL = optJOSEkid.get();
      final Optional<PublicKey> optPublicKey = accountDAO.getPublicKeyWith(accountURL);
      contextResult.setJsonWebKey(optPublicKey.orElseThrow(() -> new KidDoesNotExistException(accountURL)));
    }
    if (!verifySignatureOf(jwtContext, contextResult)) {
      throw new InvalidAcmeMessageException(join(contextResult.getErrors(), ' '), source);
    }

    final URI requestUri = fromCurrentRequestUri().build().toUri();
    try {
      final URI joseUri = contextResult.mustHave(JOSEurl.INSTANCE).toURI();
      if (!joseUri.equals(requestUri)) {
        throw new InvalidAcmeMessageException("Content of url JOSE header '" + joseUri + "' does not match request URL " + "'" + requestUri + "'", source);
      }
    } catch (URISyntaxException e) {
      throw new RuntimeException("Internal error", e);
    }

    contextResult.setPayload(parsePayloadFrom(jwtContext.getJwtClaims()));
    return contextResult;
  }

  private boolean verifySignatureOf(final JwtContext jwtContext, final Context<?> contextResult) {
    final JwtConsumer jwtConsumer = new JwtConsumerBuilder().setVerificationKey(contextResult.getKeyToVerifyJws())
            .build();
    try {
      jwtConsumer.processContext(jwtContext);
    } catch (InvalidJwtException e) {
      contextResult.appendErrorMessage("Failed signature validation: " + e.getMessage());
      return false;
    }

    return true;
  }

  /**
   * @param jwtClaims Required
   * @return Never <code>null</code>
   */
  protected abstract PAYLOAD parsePayloadFrom(JwtClaims jwtClaims) throws InvalidAcmeMessageException;

  public interface JoseHeaderPredicate extends BiPredicate<JwtContext, Context<?>> {

    // Intentionally left empty

  }

  public interface JoseHeader<TYPE_OF_VALUE> extends JoseHeaderPredicate {

    /**
     * @return Required
     */
    String contextKey();

    /**
     * @return Required
     */
    Class<TYPE_OF_VALUE> typeOfContextValue();

  }

  @NotThreadSafe
  public static final class Context<PAYLOAD> {

    private Map<String, Object> values = new HashMap<>();

    private Set<String> errors = new HashSet<>();
    private PublicKey keyToVerifyJws;
    private PAYLOAD payload;

    /**
     * @param key    Required
     * @param type   Required
     * @param <TYPE> Type of value
     * @return Never <code>null</code>
     */
    @NonNull
    public <TYPE> Optional<TYPE> get(final String key, final Class<? extends TYPE> type) {
      Validate.notEmpty(key, "Missing key");
      return ofNullable(values.get(key)).map(type::cast);
    }

    /**
     * @param header Required
     * @param <TYPE> Type of value
     * @return Never <code>null</code>
     * @throws RuntimeException If requested header is NOT available!
     */
    @NonNull
    public <TYPE> TYPE mustHave(final JoseHeader<TYPE> header) throws RuntimeException {
      Validate.notNull(header, "Missing header");
      return has(header).orElseThrow(() -> new RuntimeException("Internal error: Missing key '" + header.contextKey()
              + "'"));
    }

    /**
     * @param header Required
     * @param <TYPE> Type of value
     * @return Never <code>null</code>
     */
    @NonNull
    public <TYPE> Optional<TYPE> has(final JoseHeader<TYPE> header) {
      Validate.notNull(header, "Missing header");
      return get(header.contextKey(), header.typeOfContextValue());
    }

    /**
     * @param header Required
     * @param value  Optional
     * @param <TYPE> Type of value
     */
    public <TYPE> void put(final JoseHeader<TYPE> header, final TYPE value) {
      Validate.notNull(header, "Missing key");
      values.put(header.contextKey(), value);
    }

    /**
     * @return Never <code>null</code>
     * @throws IllegalStateException If payload has not been made available.
     */
    @NonNull
    public PAYLOAD getPayload() throws IllegalStateException {
      if (payload == null) {
        throw new IllegalStateException("Payload has not been set yet after parsing of request's payload");
      }

      return payload;
    }

    /**
     * @param payload Required
     * @throws IllegalStateException If payload has not been made available yet.
     */
    public void setPayload(@NotNull final PAYLOAD payload) {
      Validate.notNull(payload, "Missing payload");
      LOG.info("Parsed payload: {}", payload);
      this.payload = payload;
    }

    /**
     * @param keyToVerifyJws Required
     */
    public void setJsonWebKey(@NotNull final PublicKey keyToVerifyJws) {
      Validate.notNull(keyToVerifyJws, "Missing JSON web key");
      this.keyToVerifyJws = keyToVerifyJws;
    }

    /**
     * @return Never <code>null</code>
     * @throws IllegalStateException If public key has not been made available yet.
     */
    @NonNull
    public PublicKey getKeyToVerifyJws() throws IllegalStateException {
      if (keyToVerifyJws == null) {
        throw new IllegalStateException("Public-key to verify JWS request has not been set yet after parsing JOSE-header of ACME request");
      }

      return keyToVerifyJws;
    }

    /**
     * @param message Required to be not empty.
     */
    public void appendErrorMessage(final String message) {
      Validate.notEmpty(message, "Missing error message");
      this.errors.add(message);
    }

    /**
     * @return Never <code>null</code> but unmodifiable.
     */
    @NonNull
    public Set<String> getErrors() {
      return unmodifiableSet(errors);
    }

  }

  /**
   * Parses JWS-content in <a href="https://tools.ietf.org/html/rfc7515#section-7.1">Compact Serialization</a> format.
   */
  @Immutable
  public static final class CompactJwsSerializationToJwtContextConverter implements Converter<String, JwtContext> {

    public static final Converter<String, JwtContext> INSTANCE = new
            CompactJwsSerializationToJwtContextConverter();

    private static final JwtConsumer NOT_VALIDATING_JWT_CONSUMER = new JwtConsumerBuilder()
            .setSkipAllValidators()
            .setDisableRequireSignature()
            .setSkipSignatureVerification()
            .build();

    @Override
    public JwtContext convert(final String compactJwsSerialization) {
      try {
        return NOT_VALIDATING_JWT_CONSUMER.process(compactJwsSerialization);
      } catch (InvalidJwtException e) {
        throw new InvalidAcmeMessageException("Invalid JWT", compactJwsSerialization, e);
      }
    }

    @Override
    public String toString() {
      return "Compact serialization of application/jws converter";
    }
  }

  /**
   * Parses JWS in plain JSON format ... unspecified by RFC, but debugging-friendly.
   */
  @Immutable
  public final static class PlainJsonJwsSerializationToJwtContextConverter implements Converter<String, JwtContext> {

    public static final Converter<String, JwtContext> INSTANCE = new
            PlainJsonJwsSerializationToJwtContextConverter();

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Override
    public JwtContext convert(final String plainJsonJws) throws InvalidAcmeMessageException {
      LOG.debug("Converting Plain JSON: {}", plainJsonJws);
      final String compactJwsSerialization;
      try {
        final JsonNode jsonRootNode = OBJECT_MAPPER.readTree(plainJsonJws);
        compactJwsSerialization = encodeUtf8ByteRepresentation(OBJECT_MAPPER.writeValueAsString
                (jsonRootNode.get("protected")))
                + "." + encodeUtf8ByteRepresentation(OBJECT_MAPPER.writeValueAsString(jsonRootNode.get("payload"))) +
                "." + encodeUtf8ByteRepresentation(OBJECT_MAPPER.writeValueAsString(jsonRootNode.get("signature")));
      } catch (IOException e) {
        throw new InvalidAcmeMessageException("Invalid JWT", plainJsonJws, e);
      }

      return CompactJwsSerializationToJwtContextConverter.INSTANCE.convert(compactJwsSerialization);
    }

    @Override
    public String toString() {
      return "Plain JSON serialization of application/jws converter";
    }
  }

  /**
   * Parses JWS-content in <a href="https://tools.ietf.org/html/rfc7515#section-7.2.2">Flattened JWS JSON Serialization</a>
   * format.
   */
  @Immutable
  public final static class FlattenedJwsJsonSerializationToJwtContextConverter implements Converter<String, JwtContext> {

    public static final Converter<String, JwtContext> INSTANCE = new
            FlattenedJwsJsonSerializationToJwtContextConverter();

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Override
    public JwtContext convert(final String flattenedJwsJson) throws InvalidAcmeMessageException {
      LOG.debug("Converting Flattened JSON JWS: {}", flattenedJwsJson);
      final String compactJwsSerialization;
      try {
        final JsonNode jsonRootNode = OBJECT_MAPPER.readTree(flattenedJwsJson);
        final String protectedValue = jsonRootNode.get("protected").asText();
        final String payload = jsonRootNode.get("payload").asText();
        final String signature = jsonRootNode.get("signature").asText();
        compactJwsSerialization = protectedValue + "." + payload + "." + signature;
      } catch (IOException e) {
        throw new InvalidAcmeMessageException("Invalid JWT", flattenedJwsJson, e);
      }

      return CompactJwsSerializationToJwtContextConverter.INSTANCE.convert(compactJwsSerialization);
    }

    @Override
    public String toString() {
      return "Flattened JSON serialization of application/jws converter";
    }
  }

}
