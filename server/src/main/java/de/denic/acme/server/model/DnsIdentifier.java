/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.Validate;
import org.xbill.DNS.Name;
import org.xbill.DNS.TextParseException;

import javax.annotation.concurrent.Immutable;
import java.net.IDN;

@Immutable
public final class DnsIdentifier implements Identifier {

  public static final Type TYPE = new Type("dns");

  private static final boolean DO_OMIT_FINAL_DOT = true;

  private final Name name;

  /**
   * @param value Required to be not empty.
   */
  public DnsIdentifier(final String value) throws IllegalArgumentException {
    Validate.notEmpty(value, "Empty DNS identifier value");
    final Name tempName;
    try {
      tempName = Name.fromString(value, Name.root);
      this.name = Name.fromString(IDN.toASCII(tempName.toString(), IDN.USE_STD3_ASCII_RULES));
    } catch (TextParseException e) {
      throw new IllegalArgumentException("DNS identifier value '" + value + "'", e);
    }

    Validate.isTrue(!this.name.isWild(), "Wildcard DNS identifier value '%s'", value);
  }

  @Override
  public Type getType() {
    return TYPE;
  }

  @Override
  public String getValue() {
    return name.toString(DO_OMIT_FINAL_DOT);
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonIgnore
  public Name getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    DnsIdentifier that = (DnsIdentifier) o;

    return name.equals(that.name);
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

  @Override
  public String toString() {
    return "DnsIdentifier{" +
            "name=" + name +
            '}';
  }

  public static final class Factory implements IdentifierFactory<DnsIdentifier> {

    @Override
    public boolean createsType(final Type type) {
      return TYPE.equals(type);
    }

    @Override
    public DnsIdentifier createApplying(final String value) throws IllegalArgumentException {
      return new DnsIdentifier(value);
    }
  }
}
