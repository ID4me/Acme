/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.common;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.util.Base64Utils;

import javax.annotation.concurrent.Immutable;
import java.util.Random;

@Immutable
public final class RandomIdUtils {

  private static final Random RANDOM = new Random();

  /**
   * Taken from <a href="https://stackoverflow.com/questions/4485128/how-do-i-convert-long-to-byte-and-back-in-java">here</a>
   */
  public static final byte[] systemTimeBytes() {
    long input = System.currentTimeMillis();
    final byte[] result = new byte[Long.BYTES];
    for (int i = 7; i >= 0; i--) {
      result[i] = (byte) (input & 0xFF);
      input >>= 8;
    }
    return result;
  }

  /**
   * @param randomByteCount Amount of random bytes to apply
   * @return Array of bytes with size [<code>randomByteCount</code> + 8]
   */
  public static final String randomIdWithTimestampBase64UrlEncoded(final int randomByteCount) {
    final byte[] timestampBytes = systemTimeBytes();
    final byte[] randomBytes = new byte[randomByteCount];
    RANDOM.nextBytes(randomBytes);
    final byte[] bytesToEncode = ArrayUtils.addAll(randomBytes, timestampBytes);
    return Base64Utils.encodeToUrlSafeString(bytesToEncode);
  }

}
