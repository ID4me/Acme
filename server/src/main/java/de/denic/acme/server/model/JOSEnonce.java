/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.model;

import de.denic.acme.server.model.AcmeRequest.Context;
import de.denic.acme.server.model.AcmeRequest.JoseHeaderPredicate;
import de.denic.acme.server.repo.NonceDAO;
import org.apache.commons.lang3.Validate;
import org.jose4j.jwt.consumer.JwtContext;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;
import java.util.Optional;

@Immutable
public final class JOSEnonce implements JoseHeaderPredicate {

  public static final String NONCE = "nonce";

  private final NonceDAO nonceDAO;

  /**
   * @param nonceDAO Required
   */
  public JOSEnonce(final NonceDAO nonceDAO) {
    Validate.notNull(nonceDAO, "Missing nonce DAO");
    this.nonceDAO = nonceDAO;
  }

  @Override
  public boolean test(final JwtContext inputContext, final Context<?> resultContext) {
    final Optional<Nonce> optNonce = inputContext.getJoseObjects().stream().map(jws -> jws.getHeader(NONCE))
            .filter(Objects::nonNull).map(value -> new Nonce(value)).findFirst();
    if (!optNonce.isPresent()) {
      resultContext.appendErrorMessage("JOSE header '" + NONCE + "' is missing.");
      return false;
    }

    final Nonce nonce = optNonce.get();
    if (!nonceDAO.checkAndForget(nonce)) {
      resultContext.appendErrorMessage("Nonce '" + nonce + "' not valid.");
      return false;
    }

    return true;
  }

}
