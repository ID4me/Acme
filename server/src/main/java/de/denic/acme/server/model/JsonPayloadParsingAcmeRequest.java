/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.model;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import de.denic.acme.server.common.AcmeMessageException;
import de.denic.acme.server.common.InvalidAcmeMessageException;
import de.denic.acme.server.repo.AccountDAO;
import de.denic.acme.server.repo.NonceDAO;
import org.apache.commons.lang3.Validate;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.JwtContext;
import org.springframework.core.convert.converter.Converter;

import javax.annotation.concurrent.Immutable;
import java.io.IOException;

@Immutable
public abstract class JsonPayloadParsingAcmeRequest<PAYLOAD> extends AcmeRequest<PAYLOAD> {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  private final ObjectReader objectReader;

  /**
   * @param payloadType Required
   * @see AcmeRequest#AcmeRequest(AccountDAO, NonceDAO, Converter, JoseHeaderPredicate...)
   */
  protected JsonPayloadParsingAcmeRequest(final AccountDAO accountDAO, final NonceDAO nonceDAO, final Class<PAYLOAD>
          payloadType, final Converter<String, JwtContext> inputToJwtContextConverter, final
                                          JoseHeaderPredicate... joseHeaderPredicates) {
    super(accountDAO, nonceDAO, inputToJwtContextConverter, joseHeaderPredicates);
    Validate.notNull(payloadType, "Missing payload type");
    objectReader = OBJECT_MAPPER.readerFor(payloadType);
  }

  @Override
  protected final PAYLOAD parsePayloadFrom(final JwtClaims jwtClaims) throws InvalidAcmeMessageException {
    try {
      return objectReader.readValue(jwtClaims.toJson());
    } catch (JsonMappingException e) {
      final Throwable cause = e.getCause();
      if (cause instanceof AcmeMessageException) {
        throw (AcmeMessageException) cause;
      }
      throw new InvalidAcmeMessageException("Invalid payload", jwtClaims.toJson(), e);
    } catch (IOException e) {
      throw new InvalidAcmeMessageException("Invalid payload", jwtClaims.toJson(), e);
    }
  }

}
