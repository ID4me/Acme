/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.repo.impl;

import de.denic.acme.server.model.Nonce;
import de.denic.acme.server.repo.NonceDAO;
import io.micrometer.core.instrument.Metrics;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.ThreadSafe;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

import static java.time.ZonedDateTime.now;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

@Component
@ThreadSafe
public final class NonceDAOInMemoryImpl implements NonceDAO {

  public static final boolean NONCE_IS_UNKNOWN = false;
  public static final boolean NONCE_IS_OK = !NONCE_IS_UNKNOWN;
  public static final Duration DEFAULT_NONCE_LIFETIME = Duration.ofMinutes(30L);

  private static final Logger LOG = LoggerFactory.getLogger(NonceDAOInMemoryImpl.class);
  private static final int INITIAL_COUNTER_VALUE = 100;

  private final AtomicInteger invocationsLeftTillCleanup = new AtomicInteger(INITIAL_COUNTER_VALUE);
  private final Duration nonceLifetime;
  @GuardedBy("knownNonces")
  private final Map<Nonce, ZonedDateTime> knownNonces = new HashMap<>();

  /**
   * @param nonceLifetime Optional
   */
  public NonceDAOInMemoryImpl(final Duration nonceLifetime) {
    this.nonceLifetime = defaultIfNull(nonceLifetime, DEFAULT_NONCE_LIFETIME);
    Metrics.gauge(getClass().getSimpleName() + ".savedNonces", this, NonceDAOInMemoryImpl::amountOfSavedNonces);
  }

  /**
   * Applies {@link #DEFAULT_NONCE_LIFETIME}
   *
   * @see #NonceDAOInMemoryImpl(Duration)
   */
  public NonceDAOInMemoryImpl() {
    this(DEFAULT_NONCE_LIFETIME);
  }

  @Override
  public Nonce createRemembered() {
    final ZonedDateTime nonceExpirationTimestamp = now().plus(nonceLifetime);
    final Nonce result = new Nonce();
    synchronized (knownNonces) {
      while (knownNonces.putIfAbsent(result, nonceExpirationTimestamp) != null) {
        // Intentionally left empty
      }
    }
    if (invocationsLeftTillCleanup.updateAndGet(current -> (current > 0 ? --current : INITIAL_COUNTER_VALUE)) < 1) {
      expireNonces();
    }
    return result;
  }

  @Override
  public boolean checkAndForget(final Nonce nonce) {
    Validate.notNull(nonce, "Missing NONCE");
    final ZonedDateTime expirationOfKnownNonce;
    synchronized (knownNonces) {
      expirationOfKnownNonce = knownNonces.get(nonce);
      if (expirationOfKnownNonce == null) return NONCE_IS_UNKNOWN;

      knownNonces.remove(nonce);
    }
    return !expirationOfKnownNonce.isBefore(now());
  }

  @Override
  public long amountOfSavedNonces() {
    synchronized (knownNonces) {
      return knownNonces.size();
    }
  }

  private void expireNonces() {
    final ZonedDateTime now = now();
    synchronized (knownNonces) {
      final long initialAmount = amountOfSavedNonces();
      for (final Iterator<Entry<Nonce, ZonedDateTime>> iter = knownNonces.entrySet().iterator(); iter.hasNext(); ) {
        final Entry<Nonce, ZonedDateTime> entry = iter.next();
        if (entry.getValue().isBefore(now)) {
          iter.remove();
        }
      }
      LOG.debug("Expired NONCEs: Before {}, after {}", initialAmount, amountOfSavedNonces());
    }
  }

}
