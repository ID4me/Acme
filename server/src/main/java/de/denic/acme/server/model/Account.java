/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import java.security.PublicKey;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static java.lang.Integer.compare;
import static java.util.Collections.emptySet;
import static java.util.Collections.unmodifiableSet;

/**
 * <a href="https://tools.ietf.org/html/draft-ietf-acme-acme-08#section-7.1.2">ACME Internet Draft 8, chapter 7.1.2</a>
 */
@Immutable
public final class Account {

  private final Id id;
  private final Status status;
  private final Set<String> contacts;
  private final PublicKey publicKey;

  /**
   * @param id        Required
   * @param status    Required
   * @param publicKey Required
   * @param contacts  Optional
   */
  public Account(final Id id, final Status status, final PublicKey publicKey, final Collection<String> contacts) {
    Validate.notNull(id, "Missing ID");
    Validate.notNull(status, "Missing status");
    Validate.notNull(publicKey, "Missing public key");
    this.id = id;
    this.status = status;
    this.publicKey = publicKey;
    this.contacts = (contacts == null ? emptySet() : unmodifiableSet(new HashSet<>(contacts)));
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonIgnore
  public Id getId() {
    return id;
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonProperty("status")
  public Status getStatus() {
    return status;
  }

  /**
   * @return Never <code>null</code> but unmodifiable.
   */
  @JsonProperty("contact")
  public Set<String> getContacts() {
    return contacts;
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonIgnore
  public PublicKey getPublicKey() {
    return publicKey;
  }

  @Override
  public String toString() {
    return "Account{" +
            "id=" + id +
            ", status=" + status +
            ", contacts=" + contacts +
            '}';
  }

  public enum Status {

    valid, deactivated, revoked;

  }

  @Immutable
  public static final class Id implements Comparable<Id> {

    private final int value;

    /**
     * @param value Must be positive
     */
    public Id(final int value) {
      Validate.isTrue(value >= 0, "ID must be positive", value);
      this.value = value;
    }

    /**
     * @param value Must be positively numeric
     */
    public Id(final String value) {
      this(Integer.parseInt(value));
    }

    public int getValue() {
      return value;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (!(o instanceof Id)) return false;

      Id id = (Id) o;

      return value == id.value;
    }

    @Override
    public int compareTo(final Id other) {
      return compare(this.value, other.value);
    }

    @Override
    public int hashCode() {
      return value;
    }

    @Override
    public String toString() {
      return Integer.toString(value);
    }

  }

}
