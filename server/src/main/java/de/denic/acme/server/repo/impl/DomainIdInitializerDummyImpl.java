/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.repo.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.denic.acme.server.model.Identifier;
import de.denic.acme.server.repo.DomainIdInitializer;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.concurrent.Immutable;
import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;

import static org.springframework.http.HttpMethod.PUT;

@Immutable
public final class DomainIdInitializerDummyImpl implements DomainIdInitializer {

  private static final Logger LOG = LoggerFactory.getLogger(DomainIdInitializerDummyImpl.class);
  private static final HttpEntity<Object> NO_ENTITY = null;
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  private final RestOperations restOperations = new RestTemplate();
  private final URI magicLinkServiceBaseUri;

  /**
   * @param magicLinkServiceBaseUri Required
   */
  public DomainIdInitializerDummyImpl(final URI magicLinkServiceBaseUri) {
    Validate.notNull(magicLinkServiceBaseUri, "Missing base URI");
    this.magicLinkServiceBaseUri = magicLinkServiceBaseUri;
  }

  /**
   * @param magicLinkServiceBaseUriValue Required
   */
  public DomainIdInitializerDummyImpl(final String magicLinkServiceBaseUriValue) {
    this(URI.create(magicLinkServiceBaseUriValue));
  }

  @Override
  public URI initializationUriOf(final Identifier identifier) {
    Validate.notNull(identifier, "Missing identifier");
    final URI targetUri = UriComponentsBuilder.fromUri(magicLinkServiceBaseUri).path("/" + identifier.getValue
            ()).build().toUri();
    LOG.info("==> {}: >{}< ({})", PUT, NO_ENTITY, targetUri);
    final Instant startingAt = Instant.now();
    final ResponseEntity<String> responseEntity = restOperations.exchange(targetUri, PUT, NO_ENTITY, String.class);
    final Duration invocationDuration = Duration.between(startingAt, Instant.now());
    LOG.info("<== {} {}: >{}< ({}, elapsed {})", responseEntity.getStatusCode().value(),
            responseEntity.getStatusCode().name(), responseEntity, targetUri, invocationDuration);
    try {
      return URI.create(OBJECT_MAPPER.readTree(responseEntity.getBody()).get("magicuri").asText());
    } catch (IOException e) {
      throw new RuntimeException("Parsing response failed", e);
    }
  }

}
