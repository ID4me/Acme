/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.problem;

import java.net.URI;

/**
 * <a href="https://tools.ietf.org/html/rfc7807">Problem-JSON</a>-URIs taken from
 * <a href="https://tools.ietf.org/html/draft-ietf-acme-acme-09#section-6.6">ACME Internet Draft 9, chapter 6.6</a>.
 */
public interface AcmeProblemURIs {

  String ACME_ERROR_URI_NAMESPACE = "urn:ietf:params:acme:error";

  URI BAD_CSR = URI.create(ACME_ERROR_URI_NAMESPACE + ":badCSR");
  URI BAD_NONCE = URI.create(ACME_ERROR_URI_NAMESPACE + ":badNonce");
  URI BAD_SIGNATURE_ALGORITHM = URI.create(ACME_ERROR_URI_NAMESPACE + ":badSignatureAlgorithm");
  URI INVALID_CONTACT = URI.create(ACME_ERROR_URI_NAMESPACE + ":invalidContact");
  URI UNSUPPORTED_CONTACT = URI.create(ACME_ERROR_URI_NAMESPACE + ":unsupportedContact");
  URI EXTERNAL_ACCOUNT_REQUIRED = URI.create(ACME_ERROR_URI_NAMESPACE + ":externalAccountRequired");
  URI ACCOUNT_DOES_NOT_EXIST = URI.create(ACME_ERROR_URI_NAMESPACE + ":accountDoesNotExist");
  URI MALFORMED = URI.create(ACME_ERROR_URI_NAMESPACE + ":malformed");
  URI RATE_LIMITED = URI.create(ACME_ERROR_URI_NAMESPACE + ":rateLimited");
  URI REJECTED_IDENTIFIER = URI.create(ACME_ERROR_URI_NAMESPACE + ":rejectedIdentifier");
  URI SERVER_INTERNAL = URI.create(ACME_ERROR_URI_NAMESPACE + ":serverInternal");
  URI UNAUTHORIZED = URI.create(ACME_ERROR_URI_NAMESPACE + ":unauthorized");
  URI UNSUPPORTED_IDENTIFIER = URI.create(ACME_ERROR_URI_NAMESPACE + ":unsupportedIdentifier");
  URI USER_ACTION_REQUIRED = URI.create(ACME_ERROR_URI_NAMESPACE + ":userActionRequired");
  URI BAD_REVOCATION_REASON = URI.create(ACME_ERROR_URI_NAMESPACE + ":badRevocationReason");
  URI CAA = URI.create(ACME_ERROR_URI_NAMESPACE + ":caa"); // NOPMD
  URI DNS = URI.create(ACME_ERROR_URI_NAMESPACE + ":dns");
  URI CONNECTION = URI.create(ACME_ERROR_URI_NAMESPACE + ":connection");
  URI TLS = URI.create(ACME_ERROR_URI_NAMESPACE + ":tls");
  URI INCORRECT_RESPONSE = URI.create(ACME_ERROR_URI_NAMESPACE + ":incorrectResponse");

}
