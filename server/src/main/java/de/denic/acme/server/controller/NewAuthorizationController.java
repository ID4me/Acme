/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.controller;

import de.denic.acme.server.common.AcmeMediaTypes;
import de.denic.acme.server.model.AcmeRequest.CompactJwsSerializationToJwtContextConverter;
import de.denic.acme.server.model.AcmeRequest.Context;
import de.denic.acme.server.model.AcmeRequest.FlattenedJwsJsonSerializationToJwtContextConverter;
import de.denic.acme.server.model.*;
import de.denic.acme.server.model.Authorization.Status;
import de.denic.acme.server.model.Challenge.Builder;
import de.denic.acme.server.model.NewAuthorizationRequest.Payload;
import de.denic.acme.server.repo.AccountDAO;
import de.denic.acme.server.repo.AlreadyStoredException;
import de.denic.acme.server.repo.AuthorizationDAO;
import de.denic.acme.server.repo.NonceDAO;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.concurrent.Immutable;
import java.security.PublicKey;

import static de.denic.acme.server.model.Challenge.Status.pending;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequestUri;

@Immutable
@Controller
@RequestMapping("/newAuthz")
public class NewAuthorizationController implements AcmeMediaTypes, ControllerUriBuilder {

  private static final Logger LOG = LoggerFactory.getLogger(NewAuthorizationController.class);
  private static final int MAX_TRIES_TO_GENERATE_AUTHORIZATION = 5;

  private final NewAuthorizationRequest compactJwsNewAuthorizationRequest;
  private final NewAuthorizationRequest flattenedJwsJsonNewAuthorizationRequest;
  private final AuthorizationDAO authorizationDAO;
  private final AccountDAO accountDAO;

  /**
   * @param nonceDAO         Required
   * @param authorizationDAO Required
   * @param accountDAO       Required
   */
  public NewAuthorizationController(final NonceDAO nonceDAO, final AuthorizationDAO authorizationDAO, final AccountDAO
          accountDAO) {
    Validate.notNull(authorizationDAO, "Missing authorization DAO component");
    Validate.notNull(accountDAO, "Missing account DAO component");
    this.authorizationDAO = authorizationDAO;
    this.accountDAO = accountDAO;
    this.compactJwsNewAuthorizationRequest = new NewAuthorizationRequest
            (accountDAO, nonceDAO, CompactJwsSerializationToJwtContextConverter.INSTANCE);
    this.flattenedJwsJsonNewAuthorizationRequest = new NewAuthorizationRequest
            (accountDAO, nonceDAO, FlattenedJwsJsonSerializationToJwtContextConverter.INSTANCE);
  }

  @RequestMapping(method = POST, consumes = APPLICATION_JOSE_JSON_VALUE)
  public ResponseEntity<Authorization> consumingPostedJoseJson(@RequestBody final String requestBody) {
    return consumeWithConverter(requestBody, flattenedJwsJsonNewAuthorizationRequest);
  }

  @RequestMapping(method = POST, consumes = APPLICATION_JWS_VALUE)
  public ResponseEntity<Authorization> consumingPostedJws(@RequestBody final String requestBody) {
    return consumeWithConverter(requestBody, compactJwsNewAuthorizationRequest);
  }

  private ResponseEntity<Authorization> consumeWithConverter(final String requestBody, final NewAuthorizationRequest
          newAuthorizationRequest) {
    LOG.info("New AUTHORIZATION requested");
    final Context<Payload> context = newAuthorizationRequest.convert(requestBody);
    final PublicKey publicKey = accountDAO.getPublicKeyWith(context.mustHave(JOSEkid.INSTANCE)).orElseThrow
            (() -> new RuntimeException("Missing required key ID"));
    final Builder<Dns01Challenge> challengeBuilder = Dns01Challenge.builder(pending).randomToken();
    final UriComponentsBuilder authorizationBaseUriBuilder = authorizationResourceUriBuilderFrom(fromCurrentRequestUri());
    final Payload payload = context.getPayload();
    final Authorization authorization = createAndStoreAuthorizationApplying(payload.getIdentifier(), publicKey,
            challengeBuilder, authorizationBaseUriBuilder);
    return ResponseEntity.created(authorization.getUri()).body(authorization);
  }

  private Authorization createAndStoreAuthorizationApplying(final Identifier identifier, final PublicKey publicKey,
                                                            final Builder challengeBuilder, final UriComponentsBuilder authorizationBaseUriBuilder) {
    for (int i = 0; i < MAX_TRIES_TO_GENERATE_AUTHORIZATION; i++) {
      try {
        final Authorization authorization = new Authorization(identifier, Status.pending,
                authorizationBaseUriBuilder, challengeBuilder);
        authorizationDAO.insert(authorization, publicKey);
        return authorization;
      } catch (AlreadyStoredException e) {
        // Ignore, next try
      }
    }

    throw new RuntimeException("Internal error");
  }

}
