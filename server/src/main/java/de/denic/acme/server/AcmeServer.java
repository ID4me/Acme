/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server;

import de.denic.acme.server.dns.DnsChallengeValidator;
import de.denic.acme.server.dns.impl.DnsChallengeValidatorDnsJavaImpl;
import de.denic.acme.server.repo.DomainIdInitializer;
import de.denic.acme.server.repo.impl.DomainIdInitializerDummyImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.xbill.DNS.SimpleResolver;

import java.net.UnknownHostException;

import static org.apache.commons.lang3.SystemUtils.JAVA_RUNTIME_VERSION;
import static org.apache.commons.lang3.SystemUtils.JAVA_VM_VERSION;

@SpringBootApplication
public class AcmeServer implements WebMvcConfigurer {

  private static final Logger LOG = LoggerFactory.getLogger(AcmeServer.class);

  @Autowired
  private Environment environment;

  public static void main(String[] args) {
    SpringApplication.run(AcmeServer.class, args);
    LOG.info("Running on JRE version {}, JVM implementation version {}", JAVA_RUNTIME_VERSION, JAVA_VM_VERSION);
  }

//  @Override
//  public void addInterceptors(final InterceptorRegistry registry) {
//    registry.addInterceptor(new RequestTimingInterceptor());
//  }

  @Primary
  @Bean
  @ConditionalOnProperty(name = "dns.resolver.host")
  public DnsChallengeValidator createDnsChallengeValidator() throws UnknownHostException {
    final String dnsResolverHost = environment.getProperty("dns.resolver.host");
    LOG.info("Environment config of DNS resolver host: '{}'", dnsResolverHost);
    return new DnsChallengeValidatorDnsJavaImpl(new SimpleResolver(dnsResolverHost));
  }

  @Primary
  @Bean
  public DomainIdInitializer createDomainIdInitializer() {
    final String initServiceUrlValue = environment.getProperty("acme.init-service.url");
    return new DomainIdInitializerDummyImpl(initServiceUrlValue);
  }

}
