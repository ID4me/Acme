/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.controller;

import de.denic.acme.server.common.AcmeMediaTypes;
import de.denic.acme.server.dns.DnsChallengeValidator;
import de.denic.acme.server.model.AcmeRequest.CompactJwsSerializationToJwtContextConverter;
import de.denic.acme.server.model.AcmeRequest.Context;
import de.denic.acme.server.model.AcmeRequest.FlattenedJwsJsonSerializationToJwtContextConverter;
import de.denic.acme.server.model.*;
import de.denic.acme.server.model.Authorization.TechId;
import de.denic.acme.server.model.AuthorizeChallengeRequest.Payload;
import de.denic.acme.server.problem.AcmeProblemURIs;
import de.denic.acme.server.problem.ProblemDetail;
import de.denic.acme.server.repo.AccountDAO;
import de.denic.acme.server.repo.AuthorizationDAO;
import de.denic.acme.server.repo.DomainIdInitializer;
import de.denic.acme.server.repo.NonceDAO;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.concurrent.Immutable;
import java.net.URI;
import java.security.PublicKey;
import java.util.List;
import java.util.Optional;

import static de.denic.acme.server.problem.ProblemDetail.APPLICATION_PROBLEM_JSON;
import static java.util.Collections.emptyList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequestUri;

@Immutable
@Controller
@RequestMapping("/authz")
public class AuthorizationController implements AcmeMediaTypes, ControllerUriBuilder, AcmeProblemURIs {

  private static final Logger LOG = LoggerFactory.getLogger(AuthorizationController.class);
  private static final String NO_DETAIL = null;
  private static final URI NO_INSTANCE = null;

  private final AuthorizeChallengeRequest compactJwsAuthorizeChallengeRequest;
  private final AuthorizeChallengeRequest flattenedJwsJsonAuthorizeChallengeRequest;
  private final AuthorizationDAO authorizationDAO;
  private final DnsChallengeValidator dnsChallengeValidator;
  private final DomainIdInitializer domainIdInitializer;

  /**
   * @param accountDAO            Required
   * @param nonceDAO              Required
   * @param authorizationDAO      Required
   * @param dnsChallengeValidator Required
   * @param domainIdInitializer   Required
   */
  public AuthorizationController(final AccountDAO accountDAO, final NonceDAO nonceDAO,
                                 final AuthorizationDAO authorizationDAO, final DnsChallengeValidator dnsChallengeValidator,
                                 final DomainIdInitializer domainIdInitializer) {
    Validate.notNull(authorizationDAO, "Missing authorization DAO component");
    Validate.notNull(dnsChallengeValidator, "Missing DNS challenge validator component");
    Validate.notNull(domainIdInitializer, "Missing DomainID initializer");
    this.compactJwsAuthorizeChallengeRequest = new AuthorizeChallengeRequest
            (accountDAO, nonceDAO, CompactJwsSerializationToJwtContextConverter.INSTANCE);
    this.flattenedJwsJsonAuthorizeChallengeRequest = new
            AuthorizeChallengeRequest(accountDAO, nonceDAO, FlattenedJwsJsonSerializationToJwtContextConverter
            .INSTANCE);
    this.authorizationDAO = authorizationDAO;
    this.dnsChallengeValidator = dnsChallengeValidator;
    this.domainIdInitializer = domainIdInitializer;
    LOG.info("Applying DNS challenge validator: {}", this.dnsChallengeValidator);
  }

  @RequestMapping(value = "/{techId}", method = GET, produces = APPLICATION_JSON_VALUE)
  // TODO: Check for POST-as-GET, see chapter 6.3 of ACME draft 15: https://tools.ietf.org/html/draft-ietf-acme-acme-15#section-6.3
  public ResponseEntity<Authorization> getAuthorization(@PathVariable final TechId techId) {
    LOG.info("Received request for AUTHORIZATION '{}'", techId);
    final HttpHeaders additionalHeaders = new HttpHeaders();
    final UriComponentsBuilder authorizationControllersUri = fromCurrentRequestUri().path("/..");
    additionalHeaders.set("Link", "<" + directoryResourceUriBuilderFrom(authorizationControllersUri).build().normalize() + ">;rel=\"index\"");
    return authorizationDAO.findBy(techId).map(Pair::getLeft).map(authorization -> ok()
            .headers(additionalHeaders).body(authorization)).orElse(notFound().headers(additionalHeaders).build());
  }

  @RequestMapping(value = "/{techId}/{challengeId}", method = GET, produces = APPLICATION_JSON_VALUE)
  // TODO: Check for POST-as-GET, see chapter 6.3 of ACME draft 15: https://tools.ietf.org/html/draft-ietf-acme-acme-15#section-6.3
  public ResponseEntity<Challenge> getChallengeOfAuthorization(@PathVariable final TechId techId, @PathVariable final int challengeId) {
    LOG.info("Received request for CHALLENGE '{}' of authorization '{}'", challengeId, techId);
    final HttpHeaders additionalHeaders = new HttpHeaders();
    final UriComponentsBuilder authorizationControllersUri = fromCurrentRequestUri().path("/../..");
    additionalHeaders.set("Link", "<" + directoryResourceUriBuilderFrom(authorizationControllersUri).build().normalize() + ">;rel=\"index\"");
    final Optional<Challenge> optChallenge = searchForChallengeApplying(techId, challengeId);
    return optChallenge.map(challenge -> ok().headers(additionalHeaders).body(challenge)).orElse(notFound().headers(additionalHeaders).build());
  }

  @RequestMapping(value = "/{techId}/{challengeId}", method = POST, consumes = APPLICATION_JOSE_JSON_VALUE)
  public ResponseEntity<?> authorizeChallengeWithJoseJson(@PathVariable final TechId techId,
                                                          @PathVariable final int challengeId, @RequestBody final String requestBody) {
    return authorizeChallenge(techId, challengeId, requestBody, flattenedJwsJsonAuthorizeChallengeRequest);
  }

  @RequestMapping(value = "/{techId}/{challengeId}", method = POST, consumes = APPLICATION_JWS_VALUE)
  public ResponseEntity<?> authorizeChallengeWithJws(@PathVariable final TechId techId,
                                                     @PathVariable final int challengeId, @RequestBody final String requestBody) {
    return authorizeChallenge(techId, challengeId, requestBody, compactJwsAuthorizeChallengeRequest);
  }

  private ResponseEntity<?> authorizeChallenge(final TechId techId, final int challengeId, final String requestBody,
                                               final AuthorizeChallengeRequest authorizeChallengeRequest) {
    LOG.info("Received request to authorize CHALLENGE '{}' of authorization '{}'", challengeId, techId);
    final Optional<Pair<Authorization, PublicKey>> authorizationPublicKeyPair = authorizationDAO.findBy(techId);
    final Optional<Authorization> optAuthorization = authorizationPublicKeyPair.map(Pair::getLeft);
    final Optional<Challenge> optChallenge = optAuthorization.flatMap(authorization -> authorization.getChallengeBy(challengeId));
    if (!optChallenge.isPresent()) {
      return notFound().build();
    }

    final Context<Payload> context = authorizeChallengeRequest.convert(requestBody);
    final PublicKey authorizationsPubKey = authorizationPublicKeyPair.map(Pair::getRight).get();
    final PublicKey requestsPublicKey = context.getKeyToVerifyJws();
    if (!authorizationsPubKey.equals(requestsPublicKey)) {
      final ProblemDetail problemDetail = new ProblemDetail(UNAUTHORIZED, "Public keys of authorization and challenge does not match", HttpStatus
              .UNAUTHORIZED, NO_DETAIL, NO_INSTANCE);
      return ResponseEntity.status(BAD_REQUEST).contentType(APPLICATION_PROBLEM_JSON).body(problemDetail);
    }

    final Authorization authorization = optAuthorization.get();
    final Challenge challengeAfterSolvingAttempt = challengeGetsSolvedWith(authorization.getIdentifier(), (Dns01Challenge) optChallenge
            .get(), context.getPayload());
    authorization.doFinalize();
    if (!challengeAfterSolvingAttempt.isValid()) {
      return ok(challengeAfterSolvingAttempt);
    }

    final URI initializationUri = domainIdInitializer.initializationUriOf(authorization.getIdentifier());
    return ok().header("Link", "<" + initializationUri.toASCIIString() + ">;rel=\"create-form\"").body(challengeAfterSolvingAttempt);
  }

  private Dns01Challenge challengeGetsSolvedWith(final Identifier identifier, final Dns01Challenge challenge,
                                                 final Payload payload) {
    Validate.notNull(challenge, "Missing challenge");
    dnsChallengeValidator.validate(identifier, challenge, payload.getKeyAuthorization());
    return challenge;
  }

  private Optional<Challenge> searchForChallengeApplying(final TechId techId, final int challengeId) {
    final List<Challenge> authorizationChallenges = authorizationDAO.findBy(techId).map(authzAndKey ->
            authzAndKey.getLeft().getChallenges()).orElse(emptyList());
    try {
      return of(authorizationChallenges.get(challengeId));
    } catch (IndexOutOfBoundsException e) {
      return empty();
    }
  }

}
