/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.denic.acme.server.repo.AccountDAO;
import de.denic.acme.server.repo.NonceDAO;
import org.apache.commons.lang3.Validate;
import org.jose4j.jwt.consumer.JwtContext;
import org.springframework.core.convert.converter.Converter;

import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotEmpty;

@Immutable
public final class AuthorizeChallengeRequest extends JsonPayloadParsingAcmeRequest<AuthorizeChallengeRequest.Payload> {

  /**
   * @param accountDAO                 Required
   * @param nonceDAO                   Required
   * @param inputToJwtContextConverter Optional
   */
  public AuthorizeChallengeRequest(final AccountDAO accountDAO, final NonceDAO nonceDAO, final Converter<String,
          JwtContext> inputToJwtContextConverter) {
    super(accountDAO, nonceDAO, Payload.class, inputToJwtContextConverter, JOSEkid.INSTANCE, NoJOSEjwk.INSTANCE);
  }

  @Immutable
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static final class Payload {

    private final String keyAuthorization;

    /**
     * @param keyAuthorization Required
     */
    public Payload(@NotEmpty @JsonProperty("keyAuthorization") final String keyAuthorization) {
      Validate.notEmpty(keyAuthorization, "Missing keyAuthorization");
      this.keyAuthorization = keyAuthorization;
    }

    /**
     * @return Never <code>null</code> nor empty.
     */
    public String getKeyAuthorization() {
      return keyAuthorization;
    }

    @Override
    public String toString() {
      return "AuthorizeChallengeRequest{" +
              "keyAuthorization=" + keyAuthorization +
              '}';
    }

  }

}
