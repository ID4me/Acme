/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import de.denic.acme.server.controller.UnknownIdentifierTypeException;
import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;
import java.io.IOException;
import java.util.Optional;
import java.util.ServiceLoader;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;
import static java.util.stream.StreamSupport.stream;

@JsonPropertyOrder({"type", "value"})
@JsonDeserialize(using = Identifier.Deserializer.class)
public interface Identifier {

  @JsonGetter("type")
  Type getType();

  @JsonGetter("value")
  String getValue();

  @Immutable
  final class Deserializer extends JsonDeserializer<Identifier> {

    public static final ServiceLoader<IdentifierFactory> IDENTIFIER_FACTORY_SERVICE_LOADER = ServiceLoader.load(IdentifierFactory
            .class);
    public static final boolean NOT_PARALLEL = false;

    @Override
    public Identifier deserialize(final JsonParser parser, final DeserializationContext context) throws
            IOException, IllegalArgumentException {
      String typeValue = null, value = null;
      JsonToken currentToken = null;
      while (!(currentToken = parser.nextToken()).isStructEnd()) {
        if (FIELD_NAME.equals(currentToken)) {
          switch (parser.getCurrentName()) {
            case "type":
              typeValue = parser.nextTextValue();
              break;
            case "value":
              value = parser.nextTextValue();
              break;
            default: // Ignore
          }
        }
      }
      final Type type = new Type(typeValue);
      final Optional<IdentifierFactory> optFactory = stream(IDENTIFIER_FACTORY_SERVICE_LOADER.spliterator(), NOT_PARALLEL).filter(factory -> factory.createsType
              (type)).findFirst();
      final IdentifierFactory factory = optFactory.orElseThrow(() -> new UnknownIdentifierTypeException(type));
      return factory.createApplying(value);
    }
  }

  @Immutable
  final class Type {

    private final String value;

    /**
     * @param value Required to be not empty.
     */
    public Type(final String value) throws IllegalArgumentException {
      Validate.notEmpty(value, "Missing value");
      this.value = value;
    }

    /**
     * @return Never <code>null</code> nor empty.
     */
    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      Type type = (Type) o;

      return value.equals(type.value);
    }

    @Override
    public int hashCode() {
      return value.hashCode();
    }

    @Override
    public String toString() {
      return value;
    }
  }
}
