/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.repo;

import de.denic.acme.server.model.Account;
import de.denic.acme.server.model.Account.Status;

import java.net.URL;
import java.security.PublicKey;
import java.util.Collection;
import java.util.Optional;

public interface AccountDAO {

  /**
   * @param status    Required
   * @param publicKey Required
   * @param contacts  Optional
   * @return Never <code>null</code>
   */
  Account createWith(Status status, PublicKey publicKey, Collection<String> contacts) throws DuplicateAccountPublicKeyException;

  /**
   * @param accountId Required
   * @param contacts  Optional
   * @return Never <code>null</code>
   */
  Optional<Account> updateWith(Account.Id accountId, Collection<String> contacts);

  /**
   * @param accountURL Required
   * @return Never <code>null</code>
   */
  Optional<PublicKey> getPublicKeyWith(URL accountURL);

  /**
   * @param publicKey Required
   * @return Never <code>null</code>
   */
  Optional<Account> findWith(PublicKey publicKey);

  /**
   * @param accountId Required.
   * @return Never <code>null</code>
   */
  Optional<Account> findBy(Account.Id accountId);

  void clearAll();

}
