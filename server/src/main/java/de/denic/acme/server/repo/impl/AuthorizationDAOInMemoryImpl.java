/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.repo.impl;

import de.denic.acme.server.model.Authorization;
import de.denic.acme.server.model.Authorization.TechId;
import de.denic.acme.server.repo.AlreadyStoredException;
import de.denic.acme.server.repo.AuthorizationDAO;
import io.micrometer.core.instrument.Metrics;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.ThreadSafe;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Component
@ThreadSafe
public final class AuthorizationDAOInMemoryImpl implements AuthorizationDAO {

  private static final Logger LOG = LoggerFactory.getLogger(AuthorizationDAOInMemoryImpl.class);
  @GuardedBy("authorizationStore")
  private final Map<TechId, Pair<Authorization, PublicKey>> authorizationStore = new HashMap<>();

  public AuthorizationDAOInMemoryImpl() {
    Metrics.gauge(getClass().getSimpleName() + ".currentAuthorizations", this, AuthorizationDAOInMemoryImpl::amountOfCurrentAuthorizations);
  }

  @Override
  public void insert(final Authorization authorization, final PublicKey associatedKey) throws AlreadyStoredException {
    Validate.notNull(authorization, "Missing authorization");
    Validate.notNull(associatedKey, "Missing associated public key");
    final ImmutablePair<Authorization, PublicKey> authzAndKey = new ImmutablePair<>(authorization,
            associatedKey);
    final TechId techIdToInsert = authorization.getTechId();
    LOG.info("Inserting: {}", authzAndKey);
    final Pair<Authorization, PublicKey> alreadyStored;
    synchronized (authorizationStore) {
      alreadyStored = authorizationStore.putIfAbsent(techIdToInsert, authzAndKey);
    }
    if (alreadyStored != null) {
      throw new AlreadyStoredException(authorization);
    }
  }

  @Override
  public Optional<Pair<Authorization, PublicKey>> findBy(final TechId techId) {
    final Pair<Authorization, PublicKey> result;
    synchronized (authorizationStore) {
      result = authorizationStore.get(techId);
    }
    return ofNullable(result);
  }

  public long amountOfCurrentAuthorizations() {
    synchronized (authorizationStore) {
      return authorizationStore.size();
    }
  }

  @Override
  public void close() {
    // Nothing to do here
  }

}
