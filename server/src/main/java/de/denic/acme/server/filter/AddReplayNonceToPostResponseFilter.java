/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.filter;

import de.denic.acme.server.repo.NonceDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static de.denic.acme.server.model.Nonce.REPLAY_NONCE_HEADER;

@Component
public class AddReplayNonceToPostResponseFilter implements Filter {

  @Autowired
  private NonceDAO nonceDAO;

  @Override
  public void init(final FilterConfig config) {
    // Intentionally left empty
  }

  @Override
  public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws
          IOException, ServletException {
    if (isHttpPost(request)) {
      addReplayNonceHeaderTo(response);
    }
    chain.doFilter(request, response);
  }

  private boolean isHttpPost(final ServletRequest request) {
    return (request instanceof HttpServletRequest && "POST".equals(((HttpServletRequest) request).getMethod()));
  }

  private void addReplayNonceHeaderTo(final ServletResponse response) {
    if (response instanceof HttpServletResponse) {
      final HttpServletResponse httpResponse = (HttpServletResponse) response;
      httpResponse.setHeader(REPLAY_NONCE_HEADER, nonceDAO.createRemembered().getValue());
    }
  }

  @Override
  public void destroy() {
    // Intentionally left empty
  }

}