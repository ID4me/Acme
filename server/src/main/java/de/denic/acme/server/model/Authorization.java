/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import de.denic.acme.server.model.Challenge.Builder;
import org.apache.commons.lang3.Validate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.ThreadSafe;
import java.net.URI;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static de.denic.acme.server.common.RandomIdUtils.randomIdWithTimestampBase64UrlEncoded;
import static de.denic.acme.server.model.Authorization.Status.invalid;
import static de.denic.acme.server.model.Authorization.Status.valid;
import static java.time.ZonedDateTime.now;
import static java.time.format.DateTimeFormatter.ISO_INSTANT;
import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;
import static java.util.EnumSet.of;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

/**
 * <a href="https://tools.ietf.org/html/draft-ietf-acme-acme-08#section-7.1.4">ACME Internet Draft 8, chapter 7.1.4</a>
 */
@ThreadSafe
public final class Authorization {

  private static final Duration EXPIRATION_OF_NEW_AUTHORIZATION = Duration.ofHours(24L);
  private static final Duration EXPIRATION_OF_VALID_AUTHORIZATION = Duration.ofDays(365L);
  private static final EnumSet<Challenge.Status> VALID_OR_INVALID = of(Challenge.Status.valid, Challenge.Status.invalid);

  private final TechId techId;
  private final Identifier identifier;
  private final List<Challenge> challenges;
  private final URI uri;
  private final Object statusAndExpirationGuard = new Object();
  @GuardedBy("statusAndExpirationGuard")
  private ZonedDateTime expiration;
  @GuardedBy("statusAndExpirationGuard")
  private Status status;

  /**
   * @param techId              Optional
   * @param identifier          Required
   * @param status              Required
   * @param authzUriBuilderBase Required, base URI to build this instances URI from (like http://localhost:8080/authz);
   *                            gets cloned via {@link UriComponentsBuilder#cloneBuilder()}
   * @param challengeBuilders   Required to be not empty.
   */
  public Authorization(final TechId techId, final Identifier identifier, final Status status,
                       final UriComponentsBuilder authzUriBuilderBase, final Collection<Builder> challengeBuilders) {
    Validate.notNull(authzUriBuilderBase, "Missing base URI builder");
    Validate.notNull(identifier, "Missing identifier");
    Validate.notNull(status, "Missing optStatus");
    Validate.notEmpty(challengeBuilders, "Missing challenge builders");
    this.techId = defaultIfNull(techId, new TechId());
    this.identifier = identifier;
    this.status = status;
    this.expiration = now().plus(EXPIRATION_OF_NEW_AUTHORIZATION);
    this.uri = authzUriBuilderBase.cloneBuilder().path("/").path(this.techId.getValue()).build().normalize().toUri();
    final AtomicInteger counter = new AtomicInteger(0);
    this.challenges = unmodifiableList(challengeBuilders.stream().map(builder -> builder.with(this.uri.resolve(this
            .techId.getValue() + "/" + counter.getAndIncrement()))).collect(toList()));
  }

  /**
   * @see #Authorization(TechId, Identifier, Status, UriComponentsBuilder, Collection)
   */
  public Authorization(final Identifier identifier, final Status status, final UriComponentsBuilder authzUriBuilderBase, final Collection<Builder> challenges) {
    this(null, identifier, status, authzUriBuilderBase, challenges);
  }

  /**
   * @see #Authorization(TechId, Identifier, Status, UriComponentsBuilder, Collection)
   */
  public Authorization(final Identifier identifier, final Status status, final UriComponentsBuilder authzUriBuilderBase, final Builder... challenges) {
    this(identifier, status, authzUriBuilderBase, asList(challenges));
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonIgnore
  public TechId getTechId() {
    return techId;
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonGetter("identifier")
  public Identifier getIdentifier() {
    return identifier;
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonGetter("status")
  public Status getStatus() {
    synchronized (statusAndExpirationGuard) {
      return status;
    }
  }

  /**
   * @return Never <code>null</code> nor empty
   */
  @JsonGetter("challenges")
  public List<Challenge> getChallenges() {
    return challenges;
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonIgnore
  public ZonedDateTime getExpiration() {
    synchronized (statusAndExpirationGuard) {
      return expiration;
    }
  }

  /**
   * @see #getExpiration()
   */
  @JsonGetter("expires")
  public String getExpires() {
    synchronized (statusAndExpirationGuard) {
      return ISO_INSTANT.format(expiration);
    }
  }

  /**
   * @return Never <code>null</code>
   */
  @JsonIgnore
  public URI getUri() {
    return uri;
  }

  /**
   * @param id Zero-based index
   * @return Never <code>null</code>
   */
  public Optional<Challenge> getChallengeBy(final int id) {
    try {
      return of(challenges.get(id));
    } catch (IndexOutOfBoundsException e) {
      return empty();
    }
  }

  /**
   * "Finalize" operation according to <a href="https://tools.ietf.org/html/draft-ietf-acme-acme-08#section-7.5.1">ACME
   * Internet Draft 8, chapter 7.5.1</a>.
   */
  public void doFinalize() {
    final Set<Challenge.Status> currentChallengeStati = challenges.stream().map(Challenge::getStatus).filter
            (VALID_OR_INVALID::contains).collect(toSet());
    if (currentChallengeStati.contains(Challenge.Status.valid)) {
      setValidFor(EXPIRATION_OF_VALID_AUTHORIZATION);
    } else if (currentChallengeStati.contains(Challenge.Status.invalid)) {
      synchronized (statusAndExpirationGuard) {
        this.status = invalid;
      }
    }
  }

  /**
   * @param duration Required
   */
  public void setValidFor(final Duration duration) {
    Validate.notNull(duration, "Missing duration");
    synchronized (statusAndExpirationGuard) {
      this.expiration = now().plus(duration);
      this.status = valid;
    }
  }

  @Override
  public String toString() {
    synchronized (statusAndExpirationGuard) {
      return "Authorization{techId=" + techId + ", identifier=" + identifier +
              ", expiration=" + expiration +
              ", optStatus=" + status +
              ", challenges=" + challenges +
              '}';
    }
  }

  public enum Status {

    pending, processing, valid, invalid, revoked;

  }

  @Immutable
  public static final class TechId implements Comparable<TechId> {

    private final String value;

    /**
     * @param value Required to be not empty.
     */
    public TechId(final String value) {
      Validate.notEmpty(value, "Missing value");
      this.value = value;
    }

    public TechId() {
      this(randomIdWithTimestampBase64UrlEncoded(22));
    }

    /**
     * @return Never empty.
     */
    public String getValue() {
      return value;
    }

    @Override
    public int compareTo(TechId o) {
      return this.value.compareTo(o.value);
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (!(o instanceof TechId)) return false;

      TechId techId = (TechId) o;

      return value.equals(techId.value);
    }

    @Override
    public int hashCode() {
      return value.hashCode();
    }

    @Override
    public String toString() {
      return value;
    }

  }

}
