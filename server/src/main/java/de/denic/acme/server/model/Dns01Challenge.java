/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.Validate;
import org.jose4j.base64url.Base64Url;

import javax.annotation.concurrent.Immutable;
import java.net.URL;

import static de.denic.acme.server.common.RandomIdUtils.randomIdWithTimestampBase64UrlEncoded;

@Immutable
public final class Dns01Challenge extends Challenge<Dns01Challenge> {

  public static final int MIN_AMOUNT_OF_DECODED_TOKEN_BYTES = 32;

  private static final String TYPE = "dns-01";

  private final String token;

  /**
   * @param status Required
   * @param token  Required
   * @param url    Required
   */
  private Dns01Challenge(final Status status, final String token, final URL url) {
    super(Dns01Challenge.class, TYPE, status, url);
    this.token = token;
  }

  /**
   * @param status Required
   */
  public static Dns01ChallengeBuilder builder(final Status status) {
    return new MyBuilder(status);
  }

  /**
   * @return Never <code>null</code> nor empty
   */
  @JsonProperty("token")
  public String getToken() {
    return token;
  }

  @Override
  public String toString() {
    return "Dns01Challenge{" +
            "token='" + token + '\'' +
            "}" + super.toString();
  }

  public interface Dns01ChallengeBuilder {

    /**
     * @param token Required
     * @return Never <code>null</code>
     */
    Builder<Dns01Challenge> token(String token);

    /**
     * @param token Required to contain {@link #MIN_AMOUNT_OF_DECODED_TOKEN_BYTES} elements at least
     * @return Never <code>null</code>
     */
    Builder<Dns01Challenge> token(byte[] token);

    /**
     * @return Never <code>null</code>
     */
    Builder<Dns01Challenge> randomToken();

  }

  private static final class MyBuilder extends Builder<Dns01Challenge> implements Dns01ChallengeBuilder {

    private String token;

    /**
     * @param status Required
     */
    private MyBuilder(Status status) {
      super(status);
    }

    @Override
    public Dns01Challenge withNotNull(final URL url) {
      return new Dns01Challenge(status, token, url);
    }

    @Override
    public Builder<Dns01Challenge> token(final String token) {
      Validate.notEmpty(token, "Missing token");
      final byte[] base64DecodedToken = Base64Url.decode(token);
      Validate.isTrue(base64DecodedToken.length >= MIN_AMOUNT_OF_DECODED_TOKEN_BYTES, "Token '%s' does not decode to " + MIN_AMOUNT_OF_DECODED_TOKEN_BYTES + " bytes at least but %d", token, base64DecodedToken.length);
      this.token = token;
      return this;
    }

    @Override
    public Builder<Dns01Challenge> token(final byte[] token) {
      Validate.notNull(token, "Missing token bytes");
      return token(Base64Url.encode(token));
    }

    @Override
    public Builder<Dns01Challenge> randomToken() {
      return token(randomIdWithTimestampBase64UrlEncoded(52));
    }

  }

}
