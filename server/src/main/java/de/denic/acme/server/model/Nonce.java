/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.model;

import org.apache.commons.lang3.Validate;

import javax.annotation.concurrent.Immutable;

import static de.denic.acme.server.common.RandomIdUtils.randomIdWithTimestampBase64UrlEncoded;

/**
 * <a href="https://de.wikipedia.org/wiki/Nonce">Nonce</a>.
 */
@Immutable
public final class Nonce {

  public static final String REPLAY_NONCE_HEADER = "Replay-Nonce";

  private final String value;
  private final int hashCode;

  /**
   * @param value Required to be not empty.
   */
  public Nonce(final String value) {
    Validate.notEmpty(value, "Missing value");
    this.value = value;
    this.hashCode = calcHashCode();
  }

  /**
   * Applies 16 random bytes followed by 8 bytes from epoch seconds.
   */
  public Nonce() {
    this(randomIdWithTimestampBase64UrlEncoded(16));
  }

  /**
   * @return Never <code>null</code>
   */
  public String getValue() {
    return value;
  }

  private int calcHashCode() {
    return value.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Nonce nonce = (Nonce) o;

    return value.equals(nonce.value);
  }

  @Override
  public int hashCode() {
    return hashCode;
  }

  @Override
  public String toString() {
    return getValue();
  }

}
