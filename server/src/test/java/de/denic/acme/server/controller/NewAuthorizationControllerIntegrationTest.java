/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.denic.acme.server.AcmeServer;
import de.denic.acme.server.common.AcmeMediaTypes;
import de.denic.acme.server.model.Account;
import de.denic.acme.server.model.DnsIdentifier;
import de.denic.acme.server.model.Nonce;
import de.denic.acme.server.repo.AccountDAO;
import de.denic.acme.server.repo.NonceDAO;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import static de.denic.acme.server.model.Account.Status.valid;
import static de.denic.acme.server.problem.AcmeProblemURIs.ACCOUNT_DOES_NOT_EXIST;
import static de.denic.acme.server.problem.AcmeProblemURIs.REJECTED_IDENTIFIER;
import static de.denic.acme.server.problem.ProblemDetail.APPLICATION_PROBLEM_JSON;
import static de.denic.acme.util.TestKeyUtils.getPrivateTestKey;
import static de.denic.acme.util.TestKeyUtils.getPublicTestKey;
import static org.assertj.core.api.BDDAssertions.then;
import static org.jose4j.jws.AlgorithmIdentifiers.RSA_USING_SHA256;
import static org.junit.Assert.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AcmeServer.class, webEnvironment = RANDOM_PORT)
public class NewAuthorizationControllerIntegrationTest implements AcmeMediaTypes {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  @LocalServerPort
  private int port;

  @Autowired
  private AccountDAO accountDAO;
  @Autowired
  private NonceDAO nonceDAO;
  @Autowired
  private TestRestTemplate testRestTemplate;

  private Account presetAccount;
  private Nonce presetNonce;
  private String baseUrl;

  @Before
  public void setup() throws Exception {
    baseUrl = "http://localhost:" + port;
    presetAccount = accountDAO.createWith(valid, getPublicTestKey(), null);
    presetNonce = nonceDAO.createRemembered();
  }

  @After
  public void tearDown() {
    accountDAO.clearAll();
  }

  @Test
  public void postingDnsIdentifierAuthorizationAsJoseJson() throws Exception {
    final Map<String, Object> identifierFields = new HashMap<>();
    identifierFields.put("type", DnsIdentifier.TYPE);
    identifierFields.put("value", "example.de");
    final String targetUrlValue = baseUrl + "/newAuthz";
    final HttpEntity<String> requestEntity = prepareRequestEntityApplying(identifierFields, presetAccount.getId(), targetUrlValue);
    {
      final ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(targetUrlValue, requestEntity, String.class);
      then(responseEntity.getStatusCode()).as("Status code").isEqualTo(CREATED);
      final HttpHeaders responseHeaders = responseEntity.getHeaders();
      final URI locationResponseHeader = responseHeaders.getLocation();
      assertNotNull("Location header available", locationResponseHeader);
      then(locationResponseHeader.toString()).as("Location header value").startsWith(baseUrl + "/authz/");
      then(responseHeaders.getContentType()).as("Media type").isEqualTo(APPLICATION_JSON_UTF8);
      System.out.println("Response body: " + responseEntity.getBody());
    }
  }

  @Test
  public void postingAuthorizationWithUnknownAccountKey() throws Exception {
    final Map<String, Object> identifierFields = new HashMap<>();
    identifierFields.put("type", DnsIdentifier.TYPE);
    identifierFields.put("value", "example.de");
    final Account.Id idOfUnknownAccount = new Account.Id(42);
    final String targetUrlValue = baseUrl + "/newAuthz";
    final HttpEntity<String> requestEntity = prepareRequestEntityApplying(identifierFields, idOfUnknownAccount, targetUrlValue);
    {
      final ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(targetUrlValue, requestEntity, String.class);
      then(responseEntity.getStatusCode()).as("Status code").isEqualTo(BAD_REQUEST);
      then(responseEntity.getHeaders().getContentType()).as("Media type").isEqualTo(APPLICATION_PROBLEM_JSON);
      System.out.println("Response body: " + responseEntity.getBody());
      final JsonNode problemJsonNode = OBJECT_MAPPER.readTree(responseEntity.getBody());
      then(problemJsonNode.get("type").asText()).as("Problem type").isEqualTo
              (ACCOUNT_DOES_NOT_EXIST.toString());
    }
  }

  /**
   * See <a href="https://tools.ietf.org/html/draft-ietf-acme-acme-08#section-7.1.4">Internet-Draft ACME v8, Chapter 7.1.4</a>
   */
  @Test
  public void postingFooIdentifierAuthorization() throws Exception {
    final Map<String, Object> identifierFields = new HashMap<>();
    identifierFields.put("type", "foo");
    identifierFields.put("value", "foo-value");
    final String targetUrlValue = baseUrl + "/newAuthz";
    final HttpEntity<String> requestEntity = prepareRequestEntityApplying(identifierFields, presetAccount.getId(), targetUrlValue);
    {
      final ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(targetUrlValue, requestEntity, String.class);
      then(responseEntity.getStatusCode()).as("Status code").isEqualTo(BAD_REQUEST);
      then(responseEntity.getHeaders().getContentType()).as("Media type").isEqualTo(APPLICATION_PROBLEM_JSON);
      System.out.println("Response body: " + responseEntity.getBody());
      final JsonNode problemJsonNode = OBJECT_MAPPER.readTree(responseEntity.getBody());
      then(problemJsonNode.get("type").asText()).as("Problem type").isEqualTo
              (REJECTED_IDENTIFIER.toString());
    }
  }

  /**
   * See <a href="https://tools.ietf.org/html/draft-ietf-acme-acme-08#section-7.1.4">Internet-Draft ACME v8, Chapter 7.1.4</a>
   */
  @Test
  public void postingWildcardDnsIdentifierAuthorization() throws Exception {
    final Map<String, Object> identifierFields = new HashMap<>();
    identifierFields.put("type", DnsIdentifier.TYPE);
    identifierFields.put("value", "*.example.de");
    final String targetUrlValue = baseUrl + "/newAuthz";
    final HttpEntity<String> requestEntity = prepareRequestEntityApplying(identifierFields, presetAccount.getId(), targetUrlValue);
    {
      final ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(targetUrlValue, requestEntity, String.class);
      then(responseEntity.getStatusCode()).as("Status code").isEqualTo(BAD_REQUEST);
      then(responseEntity.getHeaders().getContentType()).as("Media type").isEqualTo(APPLICATION_PROBLEM_JSON);
      System.out.println("Response body: " + responseEntity.getBody());
    }
  }

  /**
   * See <a href="https://tools.ietf.org/html/draft-ietf-acme-acme-14#section-7.1.4">Internet-Draft ACME v14, Chapter 7.1.4</a>
   */
  @Test
  public void postingNonASCIIEncodedDnsIdentifierAuthorization() throws Exception {
    final Map<String, Object> identifierFields = new HashMap<>();
    identifierFields.put("type", DnsIdentifier.TYPE);
    identifierFields.put("value", "äöü.example.de");
    final String targetUrlValue = baseUrl + "/newAuthz";
    final HttpEntity<String> requestEntity = prepareRequestEntityApplying(identifierFields, presetAccount.getId(), targetUrlValue);
    {
      final ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(targetUrlValue, requestEntity, String.class);
      then(responseEntity.getStatusCode()).as("Status code").isEqualTo(BAD_REQUEST);
      then(responseEntity.getHeaders().getContentType()).as("Media type").isEqualTo(APPLICATION_PROBLEM_JSON);
      System.out.println("Response body: " + responseEntity.getBody());
    }
  }

  private HttpEntity<String> prepareRequestEntityApplying(final Map<String, Object> identifierFields, final Account.Id
          accountId, final String targetUrlValue) throws Exception {
    final JwtClaims claims = new JwtClaims();
    claims.setClaim("identifier", identifierFields);
    final JsonWebSignature jsonWebSignature = new JsonWebSignature();
    jsonWebSignature.setAlgorithmHeaderValue(RSA_USING_SHA256);
    jsonWebSignature.setKeyIdHeaderValue("http://localhost:8080/account/" + accountId.getValue());
    jsonWebSignature.setHeader("nonce", presetNonce.getValue());
    jsonWebSignature.setHeader("url", targetUrlValue);
    jsonWebSignature.setPayload(claims.toJson());
    jsonWebSignature.setKey(getPrivateTestKey());
    jsonWebSignature.sign();
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(APPLICATION_JOSE_JSON);
    return new HttpEntity<>("{\"protected\":\"" + jsonWebSignature.getHeaders()
            .getEncodedHeader() + "\",\"payload\":\"" + jsonWebSignature.getEncodedPayload() + "\",\"signature\":\"" + jsonWebSignature.getEncodedSignature() + "\"}", headers);
  }

}
