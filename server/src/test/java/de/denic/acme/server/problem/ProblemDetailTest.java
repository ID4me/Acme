/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.problem;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.net.URI;

import static org.assertj.core.api.BDDAssertions.then;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

public class ProblemDetailTest {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private static final URI TYPE = URI.create("http://problem.type");
  private static final URI INSTANCE = URI.create("http://problem.instance");

  @Test
  public void jsonSerialization() throws Exception {
    assertSerializationOf(new ProblemDetail(null, null, null, null, null), "{}");
    assertSerializationOf(new ProblemDetail(TYPE, "Problem title", BAD_REQUEST, "Detail of problem", INSTANCE),
            "{\"type\":\"http://problem.type\",\"instance\":\"http://problem.instance\",\"title\":\"Problem title\"," +
                    "\"detail\":\"Detail of problem\",\"status\":400}");
  }

  private void assertSerializationOf(final ProblemDetail CUT, final String expectedJson) throws JsonProcessingException {
    then(OBJECT_MAPPER.writeValueAsString(CUT)).as("JSON serialization of %s", CUT).isEqualTo(expectedJson);
  }

}