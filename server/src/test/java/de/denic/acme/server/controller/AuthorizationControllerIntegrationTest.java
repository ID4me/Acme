/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.denic.acme.server.AcmeServer;
import de.denic.acme.server.common.AcmeMediaTypes;
import de.denic.acme.server.model.*;
import de.denic.acme.server.model.Authorization.Status;
import de.denic.acme.server.model.Authorization.TechId;
import de.denic.acme.server.problem.AcmeProblemURIs;
import de.denic.acme.server.repo.AccountDAO;
import de.denic.acme.server.repo.AuthorizationDAO;
import de.denic.acme.server.repo.DomainIdInitializer;
import de.denic.acme.server.repo.NonceDAO;
import de.denic.acme.server.testconfig.DnsChallengeValidatorCustomizingTestConfiguration;
import de.denic.acme.util.AbstractUnitTestStartingLocalNameserver;
import org.jose4j.jwk.PublicJsonWebKey;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.shredzone.acme4j.util.KeyPairUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.net.URL;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static de.denic.acme.server.dns.impl.DnsChallengeValidatorDnsJavaImplIntegrationTest.AUTHORIZATION_TOKEN;
import static de.denic.acme.server.model.Challenge.Status.*;
import static de.denic.acme.server.problem.ProblemDetail.APPLICATION_PROBLEM_JSON;
import static de.denic.acme.util.TestKeyUtils.getPrivateTestKey;
import static de.denic.acme.util.TestKeyUtils.getPublicTestKey;
import static java.time.ZonedDateTime.now;
import static java.time.format.DateTimeFormatter.ISO_ZONED_DATE_TIME;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.assertj.core.api.BDDAssertions.then;
import static org.jose4j.jwk.PublicJsonWebKey.Factory.newPublicJwk;
import static org.jose4j.jws.AlgorithmIdentifiers.RSA_USING_SHA256;
import static org.jose4j.lang.HashUtil.SHA_256;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AcmeServer.class, webEnvironment = RANDOM_PORT)
@Import(DnsChallengeValidatorCustomizingTestConfiguration.class)
public class AuthorizationControllerIntegrationTest extends AbstractUnitTestStartingLocalNameserver implements AcmeMediaTypes {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  @LocalServerPort
  private int port;

  @MockBean
  private DomainIdInitializer domainIdInitializerMock;

  @Autowired
  private TestRestTemplate testRestTemplate;
  @Autowired
  private AuthorizationDAO authorizationDAO;
  @Autowired
  private NonceDAO nonceDAO;
  @Autowired
  private AccountDAO accountDAO;

  private Authorization presetAuthorization;
  private Nonce presetNonce;
  private Account presetAccount;
  private String baseUrl;
  private Identifier identifier;

  @Before
  public void setup() throws Exception {
    initMocks(this);
    baseUrl = "http://localhost:" + port;
    identifier = new DnsIdentifier("domain-id.test");
    presetAuthorization = new Authorization(identifier, Status.pending,
            UriComponentsBuilder.fromUriString("http://localhost:" + port + "/authz/"), Dns01Challenge
            .builder(pending).randomToken());
    authorizationDAO.insert(presetAuthorization, getPublicTestKey());
    presetNonce = nonceDAO.createRemembered();
    presetAccount = accountDAO.createWith(Account.Status.valid, getPublicTestKey(), emptyList());
  }

  @After
  public void tearDown() {
    accountDAO.clearAll();
  }

  @Test
  public void requestForUnknownAuthorization() {
    final ResponseEntity<String> responseEntity = testRestTemplate.getForEntity(baseUrl + "/authz/UNKNOWN", String.class);
    assertNotFoundStatusCodeAsWellAsEmptyBody(responseEntity);
    assertIndexLinkHeaderWithin(responseEntity);
  }

  @Test
  public void requestForKnownAuthorization() throws Exception {
    final ResponseEntity<String> responseEntity = testRestTemplate.getForEntity(baseUrl + "/authz/" + presetAuthorization
            .getTechId(), String.class);
    then(responseEntity.getStatusCode()).as("Status code").isEqualTo(OK);
    assertIndexLinkHeaderWithin(responseEntity);
    final String responseBody = responseEntity.getBody();
    System.out.println("Response body: " + responseBody);
    then(responseBody).as("Identifier of presetAuthorization").contains("\"identifier\":{\"type\":\"" + presetAuthorization.getIdentifier().getType() + "\",\"value\":\"" + presetAuthorization.getIdentifier().getValue() + "\"}");
    final JsonNode responseBodyNode = OBJECT_MAPPER.readTree(responseBody);
    then(responseBodyNode.get("status").asText()).as("Status of presetAuthorization").isEqualTo(presetAuthorization
            .getStatus().name());
    then(responseBodyNode.get("expires").asText()).as("Expiration timestamp")
            .containsPattern("\\d{4}\\-\\d{2}\\-\\d{2}T\\d{2}:\\d{2}:\\d{2}.\\d{3,9}Z");
    final JsonNode challengesNodeArray = responseBodyNode.get("challenges");
    then(challengesNodeArray.isArray()).as("Challenges as Array").isTrue();
    then(challengesNodeArray.size()).as("Amount of challenges").isEqualTo(1);
    final JsonNode firstChallengeNode = challengesNodeArray.get(0);
    then(firstChallengeNode.get("type").asText()).as("Type of challenge").isEqualTo("dns-01");
    then(firstChallengeNode.get("status").asText()).as("Status of DNS challenge").isEqualTo(pending.name());
    then(firstChallengeNode.get("url").asText()).as("URL of DNS challenge").matches("http://localhost:" + port + "/authz/[^/]+/0");
  }

  @Test
  public void requestForChallengeOfUnknownAuthorization() {
    final ResponseEntity<String> responseEntity = testRestTemplate.getForEntity(baseUrl + "/authz/UNKNOWN/0", String
            .class);
    assertNotFoundStatusCodeAsWellAsEmptyBody(responseEntity);
    assertIndexLinkHeaderWithin(responseEntity);
  }

  @Test
  public void requestForUnknownChallengeOfKnownAuthorization() {
    final ResponseEntity<String> responseEntity = testRestTemplate.getForEntity(baseUrl + "/authz/" + presetAuthorization
            .getTechId() + "/42", String.class);
    assertNotFoundStatusCodeAsWellAsEmptyBody(responseEntity);
    assertIndexLinkHeaderWithin(responseEntity);
  }

  @Test
  public void requestPresetChallengeOfAuthorization() throws Exception {
    final ResponseEntity<String> responseEntity = testRestTemplate.getForEntity(baseUrl + "/authz/" + presetAuthorization
            .getTechId() + "/0", String.class);
    then(responseEntity.getStatusCode()).as("Status code").isEqualTo(OK);
    assertIndexLinkHeaderWithin(responseEntity);
    final String responseBody = responseEntity.getBody();
    System.out.println("Response body: " + responseBody);
    final JsonNode responseBodyNode = OBJECT_MAPPER.readTree(responseBody);
    then(responseBodyNode.get("type").asText()).as("Type of challenge").isEqualTo("dns-01");
    then(responseBodyNode.get("status").asText()).as("Any known status of DNS challenge").isIn(stream(
            Status.values()).map(Enum::name).collect(toList()));
    then(responseBodyNode.get("url").asText()).as("URL of DNS challenge").matches("http://localhost:" + port + "/authz/[^/]+/0");
  }

  @Test
  public void authorizeChallengeOfUnknownAuthorization() throws Exception {
    final String targetUrlValue = baseUrl + "/authz/UNKNOWN/0";
    final HttpEntity<String> requestEntity = prepareIdentifierRequestEntity(targetUrlValue);
    {
      final ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(targetUrlValue, requestEntity, String.class);
      assertNotFoundStatusCodeAsWellAsEmptyBody(responseEntity);
      //assertIndexLinkHeaderWithin(responseEntity);
    }
  }

  @Test
  public void authorizeUnknownChallengeOfKnownAuthorization() throws Exception {
    final String targetUrlValue = baseUrl + "/authz/" + presetAuthorization.getTechId() + "/42";
    final HttpEntity<String> requestEntity = prepareIdentifierRequestEntity(targetUrlValue);
    {
      final ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(targetUrlValue, requestEntity, String.class);
      assertNotFoundStatusCodeAsWellAsEmptyBody(responseEntity);
      //assertIndexLinkHeaderWithin(responseEntity);
    }
  }

  @Test
  public void mismatchingKeyAuthorization() throws Exception {
    assertRequestingStatusOfAuthorizationYields(presetAuthorization.getTechId(), now().minusHours(23L),
            now().plusHours(25L), pending);
    assertRequestingChallengeStatusOfAuthorizationYields(presetAuthorization.getTechId(), pending);
    final String targetUrlValue = baseUrl + "/authz/" + presetAuthorization.getTechId() + "/0";
    final HttpEntity<String> requestEntity = prepareRequestEntityWithPayload("keyAuthorization", "FooKeyAuthorization", targetUrlValue, getPrivateTestKey(), new URL("http://foo.bar/account/" + presetAccount.getId()));
    {
      final ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(targetUrlValue, requestEntity, String.class);
      System.out.println("RESPONSE: " + responseEntity);
      then(responseEntity.getStatusCode()).as("Status code").isEqualTo(OK);
      then(responseEntity.getHeaders().getContentType()).as("Content type").isEqualTo(APPLICATION_JSON_UTF8);
      //assertIndexLinkHeaderWithin(responseEntity);
      final List<String> linkHeaderValues = defaultIfNull(responseEntity.getHeaders().get("Link"),
              emptyList());
      then(linkHeaderValues.stream().anyMatch(value -> value.contains("rel=\"create-form\""))).as
              ("Link header 'create-form' available").isFalse();
      final JsonNode responseBodyNode = OBJECT_MAPPER.readTree(responseEntity.getBody());
      then(responseBodyNode.get("type").asText()).as("Type of challenge").isEqualTo("dns-01");
      then(responseBodyNode.get("status").asText()).as("Status of challenge").isEqualTo(invalid.name());
      then(responseBodyNode.get("errors").isArray()).as("Errors-array of challenge").isTrue();
      then(responseBodyNode.get("errors").size()).as("Size of errors-array of challenge").isEqualTo(1);
      final JsonNode challengeResponseBodyNode = assertRequestingChallengeStatusOfAuthorizationYields(presetAuthorization.getTechId(), invalid);
      final JsonNode challengeErrorsNode = challengeResponseBodyNode.get("errors");
      then(challengeErrorsNode.isArray()).as("Array of challenge errors").isTrue();
      then(challengeErrorsNode.size()).as("Size of challenge error array").isEqualTo(1);
      final JsonNode firstErrorNode = challengeErrorsNode.get(0);
      then(firstErrorNode.get("type").asText()).as("Type of 1st error node").isEqualTo
              ("urn:ietf:params:acme:error:incorrectResponse");
      assertRequestingStatusOfAuthorizationYields(presetAuthorization.getTechId(), now().minusHours(23L), now().plusHours(25L), invalid);
    }
  }

  @Test
  public void provideMatchingKeyAuthorization() throws Exception {
    final URI magicLinkURI = URI.create("http://localhost:27272/acme/0123456789abcdefghijklm");
    assertRequestingStatusOfAuthorizationYields(presetAuthorization.getTechId(), now().minusHours(23L),
            now().plusHours(25L), pending);
    assertRequestingChallengeStatusOfAuthorizationYields(presetAuthorization.getTechId(), pending);
    final PublicJsonWebKey publicJsonWebKey = newPublicJwk(getPublicTestKey());
    final String base64OfSha256ThumbPrintOfPublicTestKey = publicJsonWebKey.calculateBase64urlEncodedThumbprint(SHA_256);
    final String keyAuthorization = AUTHORIZATION_TOKEN + "." + base64OfSha256ThumbPrintOfPublicTestKey;
    final String targetUrlValue = baseUrl + "/authz/" + presetAuthorization.getTechId() + "/0";
    final HttpEntity<String> requestEntity = prepareRequestEntityWithPayload("keyAuthorization", keyAuthorization, targetUrlValue, getPrivateTestKey(), new URL("http://foo.bar/account/" + presetAccount.getId()));
    when(domainIdInitializerMock.initializationUriOf(identifier)).thenReturn(magicLinkURI);
    {
      final ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(targetUrlValue, requestEntity, String.class);
      then(responseEntity.getStatusCode()).as("Status code").isEqualTo(OK);
      //assertIndexLinkHeaderWithin(responseEntity);
      final List<String> linkHeaderValues = defaultIfNull(responseEntity.getHeaders().get("Link"), emptyList());
      final List<String> createFormLinkHeaderValues = linkHeaderValues.stream().filter(value -> value.contains("rel=\"create-form\"")).collect(toList());
      then(createFormLinkHeaderValues.size()).as
              ("Amount of link header 'create-form' values").isEqualTo(1);
      then(createFormLinkHeaderValues.get(0)).as
              ("Link header 'create-form' value").isEqualTo("<" + magicLinkURI + ">;rel=\"create-form\"");
      System.out.println("Challenge response body: " + responseEntity.getBody());
      final JsonNode responseBodyNode = OBJECT_MAPPER.readTree(responseEntity.getBody());
      then(responseBodyNode.get("type").asText()).as("Type of challenge").isEqualTo("dns-01");
      then(responseBodyNode.get("status").asText()).as("Status of DNS challenge").isEqualTo(valid.name());
      // TODO: Activate check of challenge URL:
      //then(responseBodyNode.get("url").asText()).as("URL of DNS challenge").matches("http://localhost:" + port + "/authz/[^/]+/0");
      final JsonNode challengeResponseBodyNode = assertRequestingChallengeStatusOfAuthorizationYields(presetAuthorization.getTechId(), valid);
      then(challengeResponseBodyNode.get("errors")).as("Errors property").isNull();
      assertRequestingStatusOfAuthorizationYields(presetAuthorization.getTechId(), now().minusDays(364L),
              now().plusDays(366L), valid);
    }
  }

  @Test
  public void provideMatchingKeyAuthorizationSignedWithMismatchingKey() throws Exception {
    final KeyPair mismatchingKeyPair = KeyPairUtils.createKeyPair(2048);
    final Account accountHoldingMismatchingPubKey = accountDAO.createWith(Account.Status.valid, mismatchingKeyPair.getPublic(), emptyList());
    assertRequestingStatusOfAuthorizationYields(presetAuthorization.getTechId(), now().minusHours(23L),
            now().plusHours(25L), pending);
    assertRequestingChallengeStatusOfAuthorizationYields(presetAuthorization.getTechId(), pending);
    final PublicJsonWebKey mismatchingPublicJsonWebKey = newPublicJwk(mismatchingKeyPair.getPublic());
    final String base64OfSha256ThumbPrintOfMismatchingPublicTestKey = mismatchingPublicJsonWebKey.calculateBase64urlEncodedThumbprint(SHA_256);
    final String keyAuthorization = AUTHORIZATION_TOKEN + "." + base64OfSha256ThumbPrintOfMismatchingPublicTestKey;
    final String targetUrlValue = baseUrl + "/authz/" + presetAuthorization.getTechId() + "/0";
    final HttpEntity<String> requestEntity = prepareRequestEntityWithPayload("keyAuthorization", keyAuthorization,
            targetUrlValue, mismatchingKeyPair.getPrivate(), new URL("http://foo.bar/account/" + accountHoldingMismatchingPubKey.getId()));
    {
      final ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(targetUrlValue, requestEntity, String
              .class);
      System.out.println("Challenge response body: " + responseEntity.getBody());
      then(responseEntity.getStatusCode()).as("Status code").isEqualTo(BAD_REQUEST);
      then(responseEntity.getHeaders().getContentType()).as("Content type").isEqualTo(APPLICATION_PROBLEM_JSON);
      final JsonNode responseBodyNode = OBJECT_MAPPER.readTree(responseEntity.getBody());
      then(responseBodyNode.get("type").asText()).as("Problem JSON type").isEqualTo(AcmeProblemURIs.UNAUTHORIZED.toASCIIString());
      then(responseBodyNode.get("status").asInt()).as("Problem JSON code").isEqualTo(UNAUTHORIZED.value());
      // TODO: Activate check of challenge URL:
      //then(responseBodyNode.get("url").asText()).as("URL of DNS challenge").matches("http://localhost:" + port + "/authz/[^/]+/0");
      //assertIndexLinkHeaderWithin(responseEntity);
      assertRequestingChallengeStatusOfAuthorizationYields(presetAuthorization.getTechId(), pending);
      assertRequestingStatusOfAuthorizationYields(presetAuthorization.getTechId(), now().minusDays(364L),
              now().plusDays(366L), pending);
    }
  }

  private HttpEntity<String> prepareIdentifierRequestEntity(final String targetUrlValue) throws Exception {
    final Map<String, Object> identifierFields = new HashMap<>();
    identifierFields.put("type", DnsIdentifier.TYPE);
    identifierFields.put("value", "example.de");
    return prepareRequestEntityWithPayload("identifier", identifierFields, targetUrlValue, getPrivateTestKey(), new URL("http://foo.bar/account/" + presetAccount.getId()));
  }

  private HttpEntity<String> prepareRequestEntityWithPayload(final String key, final Object payload, final String
          targetUrlValue, final PrivateKey privateKey, final URL kidValue)
          throws Exception {
    final JwtClaims claims = new JwtClaims();
    claims.setClaim(key, payload);
    final JsonWebSignature jsonWebSignature = new JsonWebSignature();
    jsonWebSignature.setAlgorithmHeaderValue(RSA_USING_SHA256);
    jsonWebSignature.setKeyIdHeaderValue(kidValue.toExternalForm());
    jsonWebSignature.setHeader("nonce", presetNonce.getValue());
    jsonWebSignature.setHeader("url", targetUrlValue);
    jsonWebSignature.setPayload(claims.toJson());
    jsonWebSignature.setKey(privateKey);
    jsonWebSignature.sign();
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(APPLICATION_JOSE_JSON);
    return new HttpEntity<>("{\"protected\":\"" + jsonWebSignature.getHeaders().getEncodedHeader() + "\",\"payload\":\"" + jsonWebSignature.getEncodedPayload() + "\",\"signature\":\"" + jsonWebSignature.getEncodedSignature() + "\"}", headers);
  }

  private void assertIndexLinkHeaderWithin(ResponseEntity<String> responseEntity) {
    then(responseEntity.getHeaders().getFirst("Link")).as("Index link Response Header").isEqualTo
            ("<" + baseUrl + "/directory>;rel=\"index\"");
  }

  private void assertNotFoundStatusCodeAsWellAsEmptyBody(ResponseEntity<String> responseEntity) {
    then(responseEntity.getStatusCode()).as("Status code").isEqualTo(NOT_FOUND);
    then(responseEntity.getBody()).as("Response Body").isNullOrEmpty();
  }

  private JsonNode assertRequestingChallengeStatusOfAuthorizationYields(final TechId authorizationTechId, final Challenge
          .Status... expectedStati) throws Exception {
    final ResponseEntity<String> responseEntity = testRestTemplate.getForEntity(baseUrl + "/authz/" + authorizationTechId +
            "/0", String.class);
    then(responseEntity.getStatusCode()).as("Status code").isEqualTo(OK);
    final JsonNode responseBodyNode = OBJECT_MAPPER.readTree(responseEntity.getBody());
    System.out.println("Challenge response body: " + responseEntity.getBody());
    then(responseBodyNode.get("status").asText()).as("Status of DNS challenge").isIn(stream(expectedStati).map
            (Enum::name).collect(toList()));
    return responseBodyNode;
  }

  private JsonNode assertRequestingStatusOfAuthorizationYields(final TechId authorizationTechId, final ZonedDateTime
          expiresFrom, final ZonedDateTime expiresTo, final Challenge.Status... expectedStati) throws Exception {
    final ResponseEntity<String> responseEntity = testRestTemplate.getForEntity(baseUrl + "/authz/" + authorizationTechId,
            String.class);
    then(responseEntity.getStatusCode()).as("Status code").isEqualTo(OK);
    final JsonNode responseBodyNode = OBJECT_MAPPER.readTree(responseEntity.getBody());
    System.out.println("Authorization response body: " + responseEntity.getBody());
    then(responseBodyNode.get("status").asText()).as("Status of authorization").isIn(stream(expectedStati).map
            (Enum::name).collect(toList()));
    then(ZonedDateTime.from(ISO_ZONED_DATE_TIME.parse(responseBodyNode.get("expires").asText()))).as("Expiration of " +
            "authorization").isBetween(expiresFrom, expiresTo);
    return responseBodyNode;
  }

}
