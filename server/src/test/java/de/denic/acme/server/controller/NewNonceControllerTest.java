/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.controller;

import de.denic.acme.server.model.Nonce;
import de.denic.acme.server.repo.NonceDAO;
import org.assertj.core.api.Condition;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.BDDAssertions.then;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.CacheControl.noStore;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RunWith(MockitoJUnitRunner.class)
public class NewNonceControllerTest {

  @Mock
  private NonceDAO nonceDAOMock;

  @Test
  public void responseShowsCacheControlHeader() {
    given(nonceDAOMock.createRemembered()).willReturn(new Nonce());
    {
      final ResponseEntity<String> responseEntity = new NewNonceController(nonceDAOMock).viaGet();
      then(responseEntity.getHeaders().getCacheControl()).as("Cache-control header value").isEqualTo(noStore()
              .getHeaderValue());
    }
  }

  @Test
  public void responseShowsNoContent() {
    given(nonceDAOMock.createRemembered()).willReturn(new Nonce());
    {
      final ResponseEntity<String> responseEntity = new NewNonceController(nonceDAOMock).viaGet();
      then(responseEntity.getStatusCode()).as("Response status code").isEqualTo(NO_CONTENT);
      then(responseEntity.getBody()).as("Response body").isNullOrEmpty();
    }
  }

  @Test
  public void responseShowsReplayNonceHeaderOfSufficientLength() {
    given(nonceDAOMock.createRemembered()).willReturn(new Nonce());
    {
      final ResponseEntity<String> responseEntity = new NewNonceController(nonceDAOMock).viaGet();
      final List<String> replayNonceHeaderValues = responseEntity.getHeaders().get("Replay-Nonce");
      assertNotNull("Replay-Nonce header", replayNonceHeaderValues);
      then(replayNonceHeaderValues.size()).as("Single Replay-Nonce header").isEqualTo(1);
      then(replayNonceHeaderValues.get(0)).as("Replay-Nonce of sufficient length").is(new Condition<String>() {
        @Override
        public boolean matches(String value) {
          return value.length() > 16;
        }
      });
    }
  }

}