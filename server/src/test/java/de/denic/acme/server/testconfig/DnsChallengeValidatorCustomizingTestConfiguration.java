/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.testconfig;

import de.denic.acme.server.dns.DnsChallengeValidator;
import de.denic.acme.server.dns.impl.DnsChallengeValidatorDnsJavaImpl;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.xbill.DNS.SimpleResolver;

import java.net.UnknownHostException;

@TestConfiguration
public class DnsChallengeValidatorCustomizingTestConfiguration {

  @Bean
  @Primary
  public DnsChallengeValidator applyTestDnsServerAsResolverWithDnsChallengeValidator() throws UnknownHostException {
    final SimpleResolver dnsResolver;
    { // Have a look at src/test/resources/jnamed/jnamed.conf:
      dnsResolver = new SimpleResolver("127.0.0.1"); // NOPMD
      dnsResolver.setPort(35353);
    }
    return new DnsChallengeValidatorDnsJavaImpl(dnsResolver);
  }

}
