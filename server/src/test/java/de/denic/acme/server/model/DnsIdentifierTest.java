/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.Test;
import org.xbill.DNS.Name;

import java.io.IOException;

import static org.assertj.core.api.BDDAssertions.then;
import static org.junit.Assert.fail;

public class DnsIdentifierTest {

  private static final ObjectReader CUT_READER = new ObjectMapper().readerFor(DnsIdentifier.class);
  private static final ObjectWriter CUT_WRITER = new ObjectMapper().writerFor(DnsIdentifier.class);

  @Test
  public void deserializeDnsIdentifier() throws IOException {
    final String input = "{\"type\":\"dns\",\"value\":\"foo.de\",\"foo\":\"foo-value\"}";
    {
      final DnsIdentifier CUT = CUT_READER.readValue(input);
      then(CUT.getType()).as("Type").isEqualTo(DnsIdentifier.TYPE);
      then(CUT.getValue()).as("Value").isEqualTo("foo.de");
      @SuppressWarnings("unchecked") final Name name = CUT.getName();
      then(name).as("Name").isEqualTo(Name.fromConstantString("foo.de."));
    }
  }

  @Test(expected = IllegalArgumentException.class)
  public void deserializeWildcardValue() throws IOException {
    CUT_READER.readValue("{\"type\":\"dns\",\"value\":\"*.foo.de\"}");
  }

  @Test
  public void serializeDnsIdentifier() throws IOException {
    final DnsIdentifier CUT = new DnsIdentifier("foo.de");
    {
      final String serialized = CUT_WRITER.writeValueAsString(CUT);
      then(serialized).as("JSON serialization").isEqualTo("{\"type\":\"dns\",\"value\":\"foo.de\"}");
    }
  }

  @Test
  public void createWithNonASCIICharacters() throws IOException {
    expectIllegalArgumentExceptionWith("äöü.test");
    expectIllegalArgumentExceptionWith("\\\\228\\\\246\\\\252.test");
  }

  private void expectIllegalArgumentExceptionWith(final String input) throws IOException {
    try {
      fail("Expecting IllegalArgumentException, but got: " + CUT_READER.readValue("{\"type\":\"dns\",\"value\":\"" + input +
              "\"}"));
    } catch (IllegalArgumentException e) {
      // EXPECTED
      e.printStackTrace();
    }
  }

}