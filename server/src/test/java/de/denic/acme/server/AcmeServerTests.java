/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server;

import de.denic.acme.server.dns.DnsChallengeValidator;
import de.denic.acme.server.model.Dns01Challenge;
import de.denic.acme.server.model.DnsIdentifier;
import de.denic.acme.server.model.Identifier;
import de.denic.acme.server.testconfig.DnsChallengeValidatorCustomizingTestConfiguration;
import de.denic.acme.util.AbstractUnitTestStartingLocalNameserver;
import org.jose4j.jwk.PublicJsonWebKey;
import org.jose4j.lang.JoseException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.shredzone.acme4j.Account;
import org.shredzone.acme4j.AccountBuilder;
import org.shredzone.acme4j.Authorization;
import org.shredzone.acme4j.Session;
import org.shredzone.acme4j.challenge.Challenge;
import org.shredzone.acme4j.connector.Connection;
import org.shredzone.acme4j.exception.AcmeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.net.URI;
import java.security.KeyPair;
import java.security.spec.InvalidKeySpecException;

import static de.denic.acme.server.dns.impl.DnsChallengeValidatorDnsJavaImplIntegrationTest.AUTHORIZATION_TOKEN;
import static de.denic.acme.server.model.Challenge.Status.pending;
import static de.denic.acme.server.model.Challenge.Status.valid;
import static de.denic.acme.server.model.Dns01Challenge.builder;
import static de.denic.acme.util.TestKeyUtils.getPublicTestKey;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.HOURS;
import static org.assertj.core.api.BDDAssertions.then;
import static org.jose4j.jwk.PublicJsonWebKey.Factory.newPublicJwk;
import static org.jose4j.lang.HashUtil.SHA_256;
import static org.shredzone.acme4j.Status.*;
import static org.shredzone.acme4j.connector.Resource.NEW_ACCOUNT;
import static org.shredzone.acme4j.connector.Resource.NEW_NONCE;
import static org.shredzone.acme4j.util.KeyPairUtils.createKeyPair;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@Import(DnsChallengeValidatorCustomizingTestConfiguration.class)
public class AcmeServerTests extends AbstractUnitTestStartingLocalNameserver {

  private static KeyPair KEY_PAIR;

  @LocalServerPort
  private int port;
  @Autowired
  private DnsChallengeValidator dnsChallengeValidator;

  private String baseUrl;

  @BeforeClass
  public static void setupClass() {
    KEY_PAIR = createKeyPair(2048);
  }

  @Before
  public void setup() {
    baseUrl = "http://localhost:" + port;
  }

  @Test
  public void requestNewNonce() throws AcmeException {
    final Session session = new Session(baseUrl + "/directory", KEY_PAIR);
    System.out.println("New-Nonce URL: " + session.resourceUrl(NEW_NONCE));
    then(session.getNonce()).as("Nonce BEFORE resetting it").isNull();
    try (final Connection connection = session.provider().connect()) {
      connection.resetNonce(session);
    }
    then(session.getNonce()).as("Nonce AFTER resetting it").isNotNull().hasSize(24);
  }

  @Test
  public void requestPreAuthorizationWithNewAccount() throws AcmeException, InterruptedException {
    final Session session = new Session(baseUrl + "/directory", KEY_PAIR);
    then(session.resourceUrl(NEW_ACCOUNT)).as("New-Account URL").isNotNull();
    try (final Connection connection = session.provider().connect()) {
      connection.resetNonce(session);
    }
    final Account newAccount = new AccountBuilder().create(session);
    then(newAccount).as("New-Account").isNotNull();
    then(newAccount.getStatus()).as("Status of new account").isEqualTo(VALID);
    then(newAccount.getContacts()).as("Contacts of new account").isEmpty();
    then(newAccount.getOrders()).as("Orders of account").isEmpty();
    then(newAccount.getTermsOfServiceAgreed()).as("Terms of Service agreed").isNull();
    then(newAccount.getLocation().toExternalForm()).as("Location of new Account").isEqualTo
            (baseUrl + "/account/0");
    final Authorization preAuthorization = newAccount.preAuthorizeDomain("domain-id.test");
    then(preAuthorization.getStatus()).as("Status of pre-authorization").isEqualTo(PENDING);
    then(preAuthorization.getExpires()).as("Expiration of pre-authorization").isBetween(now().plus(23L, HOURS), now
            ().plus(25L, HOURS));
    then(preAuthorization.getChallenges().size()).as("Amount of challenges").isEqualTo(1);
    final Challenge dnsChallenge = preAuthorization.findChallenge(org.shredzone.acme4j.challenge.Dns01Challenge.TYPE);
    then(dnsChallenge).as("DNS challenge").isNotNull();
    then(dnsChallenge.getStatus()).as("Status of DNS challenge").isEqualTo(PENDING);
    then(dnsChallenge.getErrors()).as("Errors of DNS challenge").isEmpty();
    then(dnsChallenge.getValidated()).as("Validation timestamp of DNS challenge").isNull();
    dnsChallenge.trigger();
    // Wait for challenge to get updated
    Thread.sleep(100L);
    {
      dnsChallenge.update();
      then(dnsChallenge.getStatus()).as("Status of DNS challenge").isEqualTo(INVALID);
      then(dnsChallenge.getValidated()).as("Validation timestamp of DNS challenge").isNull();
      then(dnsChallenge.getErrors().size()).as("Amount of errors of DNS challenge").isEqualTo(1);
    }
    {
      preAuthorization.update();
      then(preAuthorization.getStatus()).as("Status of pre-authorization").isEqualTo(INVALID);
    }
  }

  @Test
  public void checkForInjectionOfCustomizedDnsChallengeValidator() throws IOException, InvalidKeySpecException, JoseException {
    final Identifier testIdentifier = new DnsIdentifier("domain-id.test");
    final Dns01Challenge testChallenge = builder(pending).token(AUTHORIZATION_TOKEN).with(URI.create
            ("http://foo.bar"));
    final PublicJsonWebKey publicJsonWebKey = newPublicJwk(getPublicTestKey());
    final String base64OfSha256ThumbPrintOfPublicTestKey = publicJsonWebKey.calculateBase64urlEncodedThumbprint(SHA_256);
    final String keyAuthorization = AUTHORIZATION_TOKEN + "." + base64OfSha256ThumbPrintOfPublicTestKey;
    then(dnsChallengeValidator.validate(testIdentifier, testChallenge, keyAuthorization).getStatus()).as
            ("Validation of DNS challenge with TestCase's DNS server").isEqualTo(valid);
  }

}
