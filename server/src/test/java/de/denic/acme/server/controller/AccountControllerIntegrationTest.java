/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.denic.acme.server.AcmeServer;
import de.denic.acme.server.common.AcmeMediaTypes;
import de.denic.acme.server.model.Account;
import de.denic.acme.server.model.Account.Status;
import de.denic.acme.server.repo.AccountDAO;
import de.denic.acme.server.repo.NonceDAO;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.lang.JoseException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.PrivateKey;
import java.util.List;

import static de.denic.acme.server.problem.ProblemDetail.APPLICATION_PROBLEM_JSON;
import static de.denic.acme.util.TestKeyUtils.getPrivateTestKey;
import static de.denic.acme.util.TestKeyUtils.getPublicTestKey;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.BDDAssertions.then;
import static org.jose4j.jws.AlgorithmIdentifiers.RSA_USING_SHA256;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AcmeServer.class, webEnvironment = RANDOM_PORT)
public class AccountControllerIntegrationTest implements AcmeMediaTypes {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private static final List<String> NO_CONTACTS = emptyList();

  @LocalServerPort
  private int port;

  @Autowired
  private TestRestTemplate testRestTemplate;
  @Autowired
  private AccountDAO accountDAO;
  @Autowired
  private NonceDAO nonceDAO;

  private String baseUrl;
  private Account presetAccount;

  @Before
  public void setup() throws Exception {
    baseUrl = "http://localhost:" + port;
    presetAccount = accountDAO.createWith(Status.valid, getPublicTestKey(), NO_CONTACTS);
  }

  @After
  public void tearDown() {
    accountDAO.clearAll();
  }

  @Test
  public void requestForUnknownAccount() {
    final ResponseEntity<String> responseEntity = testRestTemplate.getForEntity(baseUrl + "/account/42", String.class);
    then(responseEntity.getStatusCode()).as("Status code").isEqualTo(METHOD_NOT_ALLOWED);
    then(responseEntity.getHeaders().getContentType()).as("Content type").isEqualTo(APPLICATION_JSON_UTF8);
    //assertIndexLinkHeaderWithin(responseEntity);
    final String responseBody = responseEntity.getBody();
    System.out.println("Response body: " + responseBody);
  }

  @Test
  public void requestForKnownAccount() {
    final ResponseEntity<String> responseEntity = testRestTemplate.getForEntity(baseUrl + "/account/" + presetAccount
            .getId().getValue(), String.class);
    then(responseEntity.getStatusCode()).as("Status code").isEqualTo(METHOD_NOT_ALLOWED);
    then(responseEntity.getHeaders().getContentType()).as("Content type").isEqualTo(APPLICATION_JSON_UTF8);
    //assertIndexLinkHeaderWithin(responseEntity);
    final String responseBody = responseEntity.getBody();
    System.out.println("Response body: " + responseBody);
  }

  @Test
  public void updateUnknownAccount() throws Exception {
    final JwtClaims claims = new JwtClaims();
    claims.setStringListClaim("contact", "mailto:moeller@denic.de", "tel:+496927235159");
    final String targetUrlValue = baseUrl + "/account/42";
    final JsonWebSignature jsonWebSignature = prepareJsonWebSignature(claims, targetUrlValue, baseUrl +
            "/account/" + presetAccount.getId().getValue(), getPrivateTestKey());
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(APPLICATION_JOSE_JSON);
    final HttpEntity<String> requestEntity = new HttpEntity<>("{\"protected\":\"" + jsonWebSignature.getHeaders()
            .getEncodedHeader() + "\",\"payload\":\"" + jsonWebSignature.getEncodedPayload() + "\"," +
            "\"signature\":\"" + jsonWebSignature.getEncodedSignature() + "\"}", headers);
    {
      final ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(targetUrlValue, requestEntity,
              String.class);
      System.out.println("Response body: " + responseEntity.getBody());
      then(responseEntity.getStatusCode()).as("Status code").isEqualTo(NOT_FOUND);
      then(responseEntity.getHeaders().getContentType()).as("Content type").isEqualTo(APPLICATION_PROBLEM_JSON);
      final JsonNode responseBodyNode = OBJECT_MAPPER.readTree(responseEntity.getBody());
      then(responseBodyNode.get("type").asText()).as("Type").isEqualTo("urn:ietf:params:acme:error:accountDoesNotExist");
      then(responseBodyNode.get("status").asInt()).as("Status").isEqualTo(404);
      //assertIndexLinkHeaderWithin(responseEntity);
    }
  }

  @Test
  public void updateKnownAccount() throws Exception {
    final JwtClaims claims = new JwtClaims();
    final String firstContactField = "mailto:other@denic.de";
    final String secondContactField = "tel:+496927235951";
    claims.setStringListClaim("contact", firstContactField, secondContactField);
    final String targetUrlValue = baseUrl + "/account/" + presetAccount.getId().getValue();
    final JsonWebSignature jsonWebSignature = prepareJsonWebSignature(claims, targetUrlValue, targetUrlValue, getPrivateTestKey());
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(APPLICATION_JOSE_JSON);
    final HttpEntity<String> requestEntity = new HttpEntity<>("{\"protected\":\"" + jsonWebSignature.getHeaders()
            .getEncodedHeader() + "\",\"payload\":\"" + jsonWebSignature.getEncodedPayload() + "\"," +
            "\"signature\":\"" + jsonWebSignature.getEncodedSignature() + "\"}", headers);
    {
      final ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(targetUrlValue, requestEntity, String.class);
      System.out.println("Response body: " + responseEntity.getBody());
      then(responseEntity.getStatusCode()).as("Status code").isEqualTo(OK);
      then(responseEntity.getHeaders().getContentType()).as("Content type").isEqualTo(APPLICATION_JSON_UTF8);
      final JsonNode responseBodyNode = OBJECT_MAPPER.readTree(responseEntity.getBody());
      then(responseBodyNode.get("status").asText()).as("Status").isEqualTo("valid");
      final JsonNode contactNode = responseBodyNode.get("contact");
      then(contactNode.isArray()).as("Contact field is an array").isTrue();
      then(contactNode.size()).as("Contact field size").isEqualTo(2);
      then(contactNode.get(0).asText()).as("1st contact field").isEqualTo(firstContactField);
      then(contactNode.get(1).asText()).as("2nd contact field").isEqualTo(secondContactField);
      //assertIndexLinkHeaderWithin(responseEntity);
    }
  }

  private void assertIndexLinkHeaderWithin(ResponseEntity<String> responseEntity) {
    then(responseEntity.getHeaders().getFirst("Link")).as("Index link Response Header").isEqualTo("<" + baseUrl +
            "/directory>;rel=\"index\"");
  }

  private JsonWebSignature prepareJsonWebSignature(final JwtClaims claims, final String targetUrlValue, final String keyIdValue, final
  PrivateKey privateKey) throws JoseException {
    final JsonWebSignature jsonWebSignature = new JsonWebSignature();
    jsonWebSignature.setAlgorithmHeaderValue(RSA_USING_SHA256);
    jsonWebSignature.setKeyIdHeaderValue(keyIdValue);
    jsonWebSignature.setKey(privateKey);
    jsonWebSignature.setHeader("nonce", nonceDAO.createRemembered().getValue());
    jsonWebSignature.setHeader("url", targetUrlValue);
    jsonWebSignature.setPayload(claims.toJson());
    jsonWebSignature.sign();
    return jsonWebSignature;
  }

}
