/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.dns.impl;

import de.denic.acme.server.model.Dns01Challenge;
import de.denic.acme.server.model.DnsIdentifier;
import de.denic.acme.server.model.Identifier;
import de.denic.acme.util.AbstractUnitTestStartingLocalNameserver;
import de.denic.acme.util.LoggingResolverProxy;
import org.jose4j.jwk.PublicJsonWebKey;
import org.jose4j.lang.JoseException;
import org.junit.Before;
import org.junit.Test;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.SimpleResolver;

import java.io.IOException;
import java.net.URI;
import java.security.spec.InvalidKeySpecException;

import static de.denic.acme.server.model.Challenge.Status.*;
import static de.denic.acme.util.TestKeyUtils.getPublicTestKey;
import static org.assertj.core.api.BDDAssertions.then;
import static org.jose4j.jwk.PublicJsonWebKey.Factory.newPublicJwk;
import static org.jose4j.lang.HashUtil.SHA_256;
import static org.mockito.Mockito.mockingDetails;
import static org.mockito.Mockito.spy;

public class DnsChallengeValidatorDnsJavaImplIntegrationTest extends AbstractUnitTestStartingLocalNameserver {

  public static final String AUTHORIZATION_TOKEN = "fooTokenFooTokenFooTokenFooTokenFooTokenFooToken";

  private static final URI DUMMY_URI = URI.create("http://foo.bar");

  private DnsChallengeValidatorDnsJavaImpl CUT;
  private String base64OfSha256ThumbPrintOfPublicTestKey;
  private Resolver dnsResolverSpy;

  @Before
  public void setup() throws IOException, InvalidKeySpecException, JoseException {
    final SimpleResolver dnsResolver;
    { // Have a look at src/test/resources/jnamed/jnamed.conf:
      dnsResolver = new SimpleResolver("127.0.0.1"); // NOPMD
      dnsResolver.setPort(35353);
    }
    dnsResolverSpy = spy(new LoggingResolverProxy(dnsResolver));
    CUT = new DnsChallengeValidatorDnsJavaImpl(dnsResolverSpy);
    final PublicJsonWebKey publicJsonWebKey = newPublicJwk(getPublicTestKey());
    base64OfSha256ThumbPrintOfPublicTestKey = publicJsonWebKey.calculateBase64urlEncodedThumbprint
            (SHA_256);
  }

  @Test
  public void validAcmeChallengeRecordAvailable() {
    final Identifier identifier = new DnsIdentifier("domain-id.test");
    final Dns01Challenge challenge = Dns01Challenge.builder(pending).token(AUTHORIZATION_TOKEN).with(DUMMY_URI);
    final String keyAuthorization = AUTHORIZATION_TOKEN + "." + base64OfSha256ThumbPrintOfPublicTestKey;
    {
      final Dns01Challenge validated = CUT.validate(identifier, challenge, keyAuthorization);
      then(validated.getStatus()).as("Challenge status").isEqualTo(valid);
      then(validated.getErrors()).as("Challenge errors").isEmpty();
    }
  }

  @Test
  public void mismatchingAcmeChallengeRecordAvailable() {
    final Identifier identifier = new DnsIdentifier("mismatching-acme-challenge.test");
    final Dns01Challenge challenge = Dns01Challenge.builder(pending).token(AUTHORIZATION_TOKEN).with(DUMMY_URI);
    final String keyAuthorization = AUTHORIZATION_TOKEN + "." + base64OfSha256ThumbPrintOfPublicTestKey;
    {
      final Dns01Challenge validated = CUT.validate(identifier, challenge, keyAuthorization);
      then(validated.getStatus()).as("Challenge status").isEqualTo(invalid);
      then(validated.getErrors().size()).as("Count of challenge errors").isEqualTo(1);
      then(validated.getErrors().get(0).getType().toASCIIString()).as("Type of challenge error").isEqualTo("urn:ietf:params:acme:error:incorrectResponse");
    }
  }

  @Test
  public void missingAcmeChallengeRecord() {
    final Identifier identifier = new DnsIdentifier("missing-acme-challenge.test");
    final Dns01Challenge challenge = Dns01Challenge.builder(pending).token(AUTHORIZATION_TOKEN).with(DUMMY_URI);
    final String keyAuthorization = AUTHORIZATION_TOKEN + "." + base64OfSha256ThumbPrintOfPublicTestKey;
    {
      final Dns01Challenge validated = CUT.validate(identifier, challenge, keyAuthorization);
      then(validated.getStatus()).as("Challenge status").isEqualTo(invalid);
      then(validated.getErrors().size()).as("Count of challenge errors").isEqualTo(1);
      then(validated.getErrors().get(0).getType().toASCIIString()).as("Type of challenge error").isEqualTo("urn:ietf:params:acme:error:incorrectResponse");
    }
  }

  @Test
  public void unknownDomain() {
    final Identifier identifier = new DnsIdentifier("unknown-domain.test");
    final Dns01Challenge challenge = Dns01Challenge.builder(pending).token(AUTHORIZATION_TOKEN).with(DUMMY_URI);
    final String keyAuthorization = AUTHORIZATION_TOKEN + "." + base64OfSha256ThumbPrintOfPublicTestKey;
    {
      final Dns01Challenge validated = CUT.validate(identifier, challenge, keyAuthorization);
      then(validated.getStatus()).as("Challenge status").isEqualTo(invalid);
      then(validated.getErrors().size()).as("Count of challenge errors").isEqualTo(1);
      then(validated.getErrors().get(0).getType().toASCIIString()).as("Type of challenge error").isEqualTo("urn:ietf:params:acme:error:incorrectResponse");
    }
  }

  @Test
  public void validAcmeChallengeResponsesAreNotCached() {
    final Identifier identifier = new DnsIdentifier("domain-id.test");
    final Dns01Challenge challenge = Dns01Challenge.builder(pending).token(AUTHORIZATION_TOKEN).with(DUMMY_URI);
    final String matchingKeyAuthorization = AUTHORIZATION_TOKEN + "." + base64OfSha256ThumbPrintOfPublicTestKey;
    final String mismatchingKeyAuthorization = "FooBarFooBar-Mismatching-0123456789." +
            base64OfSha256ThumbPrintOfPublicTestKey;
    {
      then(CUT.validate(identifier, challenge, matchingKeyAuthorization).getStatus()).as("First validation").isEqualTo(valid);
      then(CUT.validate(identifier, challenge, mismatchingKeyAuthorization).getStatus()).as("Second validation").isEqualTo(invalid);
      then(mockingDetails(dnsResolverSpy).getInvocations().size()).as("Count of DNS resolver invocations").isEqualTo(2);
    }
  }

  @Test
  public void validAsWellAsMismatchingAcmeChallengeRecordAvailable() {
    final Identifier identifier = new DnsIdentifier("valid-as-well-as-mismatching-acme-challenge.test");
    final Dns01Challenge challenge = Dns01Challenge.builder(pending).token(AUTHORIZATION_TOKEN).with(DUMMY_URI);
    final String keyAuthorization = AUTHORIZATION_TOKEN + "." + base64OfSha256ThumbPrintOfPublicTestKey;
    {
      final Dns01Challenge validated = CUT.validate(identifier, challenge, keyAuthorization);
      then(validated.getStatus()).as("Challenge status").isEqualTo(valid);
      then(validated.getErrors()).as("Challenge errors").isEmpty();
    }
  }

}