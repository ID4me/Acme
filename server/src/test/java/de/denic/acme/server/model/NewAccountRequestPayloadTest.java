/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.denic.acme.server.model.NewAccountRequest.Payload;
import org.junit.Test;

import java.io.IOException;
import java.util.Set;

import static org.assertj.core.api.BDDAssertions.then;

public class NewAccountRequestPayloadTest {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private static final JsonNodeFactory JSON_NODE_FACTORY = JsonNodeFactory.instance;

  @Test
  public void jsonMissingOnlyReturnExistingFlag() throws IOException {
    final ObjectNode payloadRootNode = JSON_NODE_FACTORY.objectNode();
    payloadRootNode.put("termsOfServiceAgreed", true);
    final ArrayNode contactArrayNode = payloadRootNode.putArray("contact");
    contactArrayNode.add("contact1").add("contact2");
    final String payloadJson = OBJECT_MAPPER.writeValueAsString(payloadRootNode);
    {
      final Payload readPayload = OBJECT_MAPPER.readerFor(Payload.class).readValue(payloadJson);
      then(readPayload.getOnlyReturnExisting()).as("Only-Return-Existing field").isNull();
      then(readPayload.getTermsOfServiceAgreed()).as("Terms-Of-Service-Agreed field").isTrue();
      final Set<String> contacts = readPayload.getContacts();
      then(contacts.size()).as("Contacts count").isEqualTo(2);
      then(contacts).as("Contacts").containsOnlyOnce("contact1", "contact2");
    }
  }

  @Test
  public void jsonWithOnlyReturnExistingFlagSetToFalseExplicitely() throws IOException {
    final ObjectNode payloadRootNode = JSON_NODE_FACTORY.objectNode();
    payloadRootNode.put("onlyReturnExisting", false);
    final ArrayNode contactArrayNode = payloadRootNode.putArray("contact");
    contactArrayNode.add("contact1").add("contact2");
    final String payloadJson = OBJECT_MAPPER.writeValueAsString(payloadRootNode);
    {
      final Payload readPayload = OBJECT_MAPPER.readerFor(Payload.class).readValue(payloadJson);
      then(readPayload.getOnlyReturnExisting()).as("Only-Return-Existing field").isFalse();
      then(readPayload.getTermsOfServiceAgreed()).as("Terms-Of-Service-Agreed field").isNull();
      final Set<String> contacts = readPayload.getContacts();
      then(contacts.size()).as("Contacts count").isEqualTo(2);
      then(contacts).as("Contacts").containsOnlyOnce("contact1", "contact2");
    }
  }

}