/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.denic.acme.server.AcmeServer;
import de.denic.acme.server.common.AcmeMediaTypes;
import de.denic.acme.server.repo.AccountDAO;
import de.denic.acme.server.repo.NonceDAO;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.lang.JoseException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Collection;

import static de.denic.acme.server.model.Account.Status.valid;
import static de.denic.acme.server.problem.ProblemDetail.APPLICATION_PROBLEM_JSON;
import static de.denic.acme.util.TestKeyUtils.getPrivateTestKey;
import static de.denic.acme.util.TestKeyUtils.getPublicTestKey;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.BDDAssertions.then;
import static org.jose4j.jwk.PublicJsonWebKey.Factory.newPublicJwk;
import static org.jose4j.jws.AlgorithmIdentifiers.RSA_USING_SHA256;
import static org.junit.Assert.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AcmeServer.class, webEnvironment = RANDOM_PORT)
public class NewAccountControllerIntegrationTest implements AcmeMediaTypes {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private static final Collection<String> NO_CONTACTS = emptyList();

  @LocalServerPort
  private int port;

  @Autowired
  private TestRestTemplate testRestTemplate;
  @Autowired
  private AccountDAO accountDAO;
  @Autowired
  private NonceDAO nonceDAO;

  private String baseUrl;

  @Before
  public void setup() {
    baseUrl = "http://localhost:" + port;
  }

  @After
  public void tearDown() {
    accountDAO.clearAll();
  }

  @Test
  public void postNewAccountAsJoseJson() throws Exception {
    final JwtClaims claims = new JwtClaims();
    claims.setClaim("termsOfServiceAgreed", true);
    claims.setClaim("onlyReturnExisting", false);
    claims.setStringListClaim("contact", "mailto:moeller@denic.de", "tel:+496927235159");
    final String targetUrlValue = baseUrl + "/newAccount";
    final JsonWebSignature jsonWebSignature = prepareJsonWebSignature(claims, targetUrlValue, getPublicTestKey(), getPrivateTestKey());
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(APPLICATION_JOSE_JSON);
    final HttpEntity<String> requestEntity = new HttpEntity<>("{\"protected\":\"" + jsonWebSignature.getHeaders()
            .getEncodedHeader() + "\",\"payload\":\"" + jsonWebSignature.getEncodedPayload() + "\",\"signature\":\"" + jsonWebSignature.getEncodedSignature() + "\"}", headers);
    {
      final ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(targetUrlValue, requestEntity, String.class);
      then(responseEntity.getStatusCode()).as("Status code").isEqualTo(CREATED);
      final HttpHeaders responseHeaders = responseEntity.getHeaders();
      final URI locationResponseHeader = responseHeaders.getLocation();
      assertNotNull("Location header available", locationResponseHeader);
      then(locationResponseHeader.toString()).as("Location header value").isEqualTo(baseUrl + "/account/0");
      then(responseHeaders.getContentType()).as("Media type").isEqualTo(APPLICATION_JSON_UTF8);
      final JsonNode responseBodyNode = OBJECT_MAPPER.readTree(responseEntity.getBody());
      System.out.println("Response body: " + responseEntity.getBody());
      then(responseBodyNode.get("status").asText()).as("Status").isEqualTo(valid.name());
      then(responseBodyNode.get("contact").isArray()).as("Contact field is an array").isTrue();
      then(responseBodyNode.get("contact").size()).as("Contact field size").isEqualTo(2);
      assertIndexLinkHeaderWithin(responseEntity);
    }
  }

  private JsonWebSignature prepareJsonWebSignature(final JwtClaims claims, final String targetUrlValue, final PublicKey
          publicKey, final PrivateKey privateKey) throws JoseException {
    final JsonWebSignature jsonWebSignature = new JsonWebSignature();
    jsonWebSignature.setAlgorithmHeaderValue(RSA_USING_SHA256);
    jsonWebSignature.getHeaders().setJwkHeaderValue("jwk", newPublicJwk(publicKey));
    jsonWebSignature.setKey(privateKey);
    jsonWebSignature.setHeader("nonce", nonceDAO.createRemembered().getValue());
    jsonWebSignature.setHeader("url", targetUrlValue);
    jsonWebSignature.setPayload(claims.toJson());
    jsonWebSignature.sign();
    return jsonWebSignature;
  }

  @Test
  public void findExistingAccountWithKey() throws Exception {
    final PublicKey publicTestKey = getPublicTestKey();
    accountDAO.createWith(valid, publicTestKey, NO_CONTACTS);
    final JwtClaims claims = new JwtClaims();
    claims.setClaim("onlyReturnExisting", true);
    final String targetUrlValue = baseUrl + "/newAccount";
    final JsonWebSignature jsonWebSignature = prepareJsonWebSignature(claims, targetUrlValue, publicTestKey, getPrivateTestKey());
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(APPLICATION_JOSE_JSON);
    final HttpEntity<String> requestEntity = new HttpEntity<>("{\"protected\":\"" + jsonWebSignature.getHeaders()
            .getEncodedHeader() + "\",\"payload\":\"" + jsonWebSignature.getEncodedPayload() + "\"," +
            "\"signature\":\"" + jsonWebSignature.getEncodedSignature() + "\"}", headers);
    {
      final ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(targetUrlValue, requestEntity, String.class);
      then(responseEntity.getStatusCode()).as("Status code").isEqualTo(OK);
      final HttpHeaders responseHeaders = responseEntity.getHeaders();
      final URI locationResponseHeader = responseHeaders.getLocation();
      assertNotNull("Location header available", locationResponseHeader);
      then(locationResponseHeader.toString()).as("Location header value").isEqualTo(baseUrl + "/account/0");
      then(responseHeaders.getContentType()).as("Media type").isEqualTo(APPLICATION_JSON_UTF8);
      final JsonNode responseBodyNode = OBJECT_MAPPER.readTree(responseEntity.getBody());
      then(responseBodyNode.get("status").asText()).as("Status").isEqualTo(valid.name());
      then(responseBodyNode.get("contact").isArray()).as("Contact field is an array").isTrue();
      then(responseBodyNode.get("contact").size()).as("Contact field size").isEqualTo(0);
      assertIndexLinkHeaderWithin(responseEntity);
    }
  }

  @Test
  public void findAccountWithUnknownKey() throws Exception {
    final PublicKey publicTestKey = getPublicTestKey();
    final JwtClaims claims = new JwtClaims();
    claims.setClaim("onlyReturnExisting", true);
    final String targetUrlValue = baseUrl + "/newAccount";
    final JsonWebSignature jsonWebSignature = prepareJsonWebSignature(claims, targetUrlValue, publicTestKey, getPrivateTestKey());
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(APPLICATION_JOSE_JSON);
    final HttpEntity<String> requestEntity = new HttpEntity<>("{\"protected\":\"" + jsonWebSignature.getHeaders()
            .getEncodedHeader() + "\",\"payload\":\"" + jsonWebSignature.getEncodedPayload() + "\"," +
            "\"signature\":\"" + jsonWebSignature.getEncodedSignature() + "\"}", headers);
    {
      final ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(targetUrlValue, requestEntity, String.class);
      then(responseEntity.getStatusCode()).as("Status code").isEqualTo(BAD_REQUEST);
      then(responseEntity.getHeaders().getLocation()).as("Location header").isNull();
      then(responseEntity.getHeaders().getContentType()).as("Media type").isEqualTo(APPLICATION_PROBLEM_JSON);
      final JsonNode responseBodyNode = OBJECT_MAPPER.readTree(responseEntity.getBody());
      System.out.println("Response body: " + responseEntity.getBody());
      then(responseBodyNode.get("status").asInt()).as("Status").isEqualTo(400);
      then(responseBodyNode.get("type").asText()).as("Type").isEqualTo("urn:ietf:params:acme:error:accountDoesNotExist");
      assertIndexLinkHeaderWithin(responseEntity);
    }
  }

  /**
   * See <a href="https://tools.ietf.org/html/draft-ietf-acme-acme-09#section-7.3.1">Chapter 7.3.1 of ACME Draft 9</a>
   */
  @Test
  public void rejectPostingOfNewAccountWithAlreadyKnownPubKey() throws Exception {
    final String targetUrlValue = baseUrl + "/newAccount";
    final PublicKey samePublicTestKey = getPublicTestKey();
    URI uriOfNewlyCreatedAccount;
    { // Create first Account
      final JwtClaims claims = new JwtClaims();
      claims.setClaim("termsOfServiceAgreed", true);
      claims.setStringListClaim("contact", "mailto:moeller@denic.de", "tel:+496927235159");
      final JsonWebSignature jsonWebSignature = prepareJsonWebSignature(claims, targetUrlValue, samePublicTestKey, getPrivateTestKey());
      final HttpHeaders headers = new HttpHeaders();
      headers.setContentType(APPLICATION_JOSE_JSON);
      final HttpEntity<String> requestEntity = new HttpEntity<>("{\"protected\":\"" + jsonWebSignature.getHeaders()
              .getEncodedHeader() + "\",\"payload\":\"" + jsonWebSignature.getEncodedPayload() + "\",\"signature\":\"" + jsonWebSignature.getEncodedSignature() + "\"}", headers);
      final ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(targetUrlValue, requestEntity, String.class);
      then(responseEntity.getStatusCode()).as("Status code").isEqualTo(CREATED);
      then(responseEntity.getHeaders().getContentType()).as("Content type").isEqualTo(APPLICATION_JSON_UTF8);
      uriOfNewlyCreatedAccount = responseEntity.getHeaders().getLocation();
    }
    { // Create second one
      final JwtClaims claims = new JwtClaims();
      claims.setClaim("termsOfServiceAgreed", true);
      claims.setStringListClaim("contact", "mailto:sanz@denic.de", "tel:+496927235175");
      final JsonWebSignature jsonWebSignature = prepareJsonWebSignature(claims, targetUrlValue, samePublicTestKey, getPrivateTestKey());
      final HttpHeaders headers = new HttpHeaders();
      headers.setContentType(APPLICATION_JOSE_JSON);
      final HttpEntity<String> requestEntity = new HttpEntity<>("{\"protected\":\"" + jsonWebSignature.getHeaders()
              .getEncodedHeader() + "\",\"payload\":\"" + jsonWebSignature.getEncodedPayload() + "\",\"signature\":\"" + jsonWebSignature.getEncodedSignature() + "\"}", headers);
      final ResponseEntity<String> responseEntity = testRestTemplate.postForEntity(targetUrlValue, requestEntity, String.class);
      then(responseEntity.getStatusCode()).as("Status code").isEqualTo(OK);
      then(responseEntity.getHeaders().getContentType()).as("Content type").isNull();
      then(responseEntity.getHeaders().getLocation()).as("Location header").isEqualTo(uriOfNewlyCreatedAccount);
      System.out.println("Response body: '" + responseEntity.getBody() + "'");
      then(responseEntity.getBody()).as("Response body").isNullOrEmpty();
    }
  }

  private void assertIndexLinkHeaderWithin(ResponseEntity<String> responseEntity) {
    then(responseEntity.getHeaders().getFirst("Link")).as("Index link Response Header").isEqualTo("<" + baseUrl + "/directory>;rel=\"index\"");
  }

}