/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.repo.impl;

import de.denic.acme.server.model.Nonce;
import de.denic.acme.server.repo.NonceDAO;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;

import static org.assertj.core.api.BDDAssertions.then;

public class NonceDAOInMemoryImplTest {

  public static final Duration TEST_LIFETIME = Duration.ofMillis(500L);

  private NonceDAO CUT;

  @Before
  public void setUp() throws Exception {
    CUT = new NonceDAOInMemoryImpl(TEST_LIFETIME);
  }

  @Test
  public void getRememberedNonceAndCheckItAndAnotherNonce() throws Exception {
    final Nonce rememberedNonce = CUT.createRemembered();
    then(rememberedNonce).as("Remembered NONCE").isNotNull();
    then(CUT.checkAndForget(rememberedNonce)).as("Check remembered NONCE").isTrue();
    then(CUT.checkAndForget(rememberedNonce)).as("Check remembered NONCE a second time").isFalse();
    then(CUT.checkAndForget(new Nonce())).as("Check unknown NONCE").isFalse();
  }

  @Test
  public void checkForExpiration() throws Exception {
    final Nonce rememberedNonce = CUT.createRemembered();
    then(rememberedNonce).as("Remembered NONCE").isNotNull();
    Thread.sleep(TEST_LIFETIME.multipliedBy(2L).toMillis());
    then(CUT.checkAndForget(rememberedNonce)).as("Check remembered NONCE after its expiration").isFalse();
  }

  @Test
  public void checkForCleanupOperation() throws Exception {
    then(CUT.amountOfSavedNonces()).as("Initial amount").isEqualTo(0L);
    final int initialAmount = 99;
    for (int i = 0; i < initialAmount; i++) {
      then(CUT.createRemembered()).as("Remembered NONCE %n", i).isNotNull();
    }
    then(CUT.amountOfSavedNonces()).as("Amount after requesting a lot of NONCEs").isEqualTo(initialAmount);
    Thread.sleep(TEST_LIFETIME.multipliedBy(2L).toMillis());
    then(CUT.createRemembered()).as("Remembered NONCE").isNotNull();
    then(CUT.amountOfSavedNonces()).as("Amount after cleanup").isEqualTo(1L);
  }

}