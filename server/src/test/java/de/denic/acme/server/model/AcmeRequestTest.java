/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.server.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.denic.acme.server.common.InvalidAcmeMessageException;
import de.denic.acme.server.model.AcmeRequest.Context;
import de.denic.acme.server.model.AcmeRequest.PlainJsonJwsSerializationToJwtContextConverter;
import de.denic.acme.server.repo.AccountDAO;
import de.denic.acme.server.repo.NonceDAO;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.jose4j.base64url.Base64Url;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.JwtContext;
import org.jose4j.jwx.JsonWebStructure;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.Signature;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;
import java.util.Vector;

import static de.denic.acme.util.TestKeyUtils.getPrivateTestKey;
import static de.denic.acme.util.TestKeyUtils.getPublicTestKey;
import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.fail;
import static org.assertj.core.api.BDDAssertions.then;
import static org.jose4j.base64url.Base64Url.encodeUtf8ByteRepresentation;
import static org.jose4j.jwk.PublicJsonWebKey.Factory.newPublicJwk;
import static org.jose4j.jws.AlgorithmIdentifiers.RSA_USING_SHA256;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AcmeRequestTest {

  private static final Enumeration<String> EMPTY_HEADER_VALUE_ENUMERATION = new Vector<String>(0).elements();
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  private static final JsonNodeFactory JSON_NODE_FACTORY = JsonNodeFactory.instance;
  private static final String DUMMY_URL_VALUE = "http://dummy.url";

  @Mock
  private NonceDAO nonceDAOMock;
  @Mock
  private AccountDAO accountDAOMock;
  @Mock
  private HttpServletRequest requestMock;

  @Before
  public void setup() {
    when(requestMock.getRequestURL()).thenReturn(new StringBuffer(DUMMY_URL_VALUE));
    when(requestMock.getHeaderNames()).thenReturn(EMPTY_HEADER_VALUE_ENUMERATION);
    RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(requestMock));
  }

  @Test
  public void conversionSucceedsProvidingKeyId() throws Exception {
    final Nonce knownNonce = new Nonce();
    given(nonceDAOMock.checkAndForget(knownNonce)).willReturn(true);
    final URL kidValue = new URL("http://key.id");
    given(accountDAOMock.getPublicKeyWith(kidValue)).willReturn(of(getPublicTestKey()));
    final ObjectNode protectedNode = JSON_NODE_FACTORY.objectNode();
    protectedNode.put("kid", kidValue.toExternalForm());
    protectedNode.put("nonce", knownNonce.getValue());
    protectedNode.put("url", DUMMY_URL_VALUE);
    final ObjectNode payloadNode = JSON_NODE_FACTORY.objectNode();
    final String jsonInput = compactSerializationOf(protectedNode, payloadNode, getPrivateTestKey());
    {
      final Context<Pair<String, String>> context = new MyAcmeRequest(new JOSEkid(), new NoJOSEjwk()).convert(jsonInput);
      then(context.getKeyToVerifyJws()).as("JOSE jwk available").isNotNull();
      final Optional<URL> optKid = context.has(JOSEkid.INSTANCE);
      then(optKid.isPresent()).as("JOSE kid available").isTrue();
      then(optKid.get()).as("JOSE kid value").isEqualTo(kidValue);
    }
  }

  @Test
  public void conversionFailedDueToAvailableJwkField() throws Exception {
    final ObjectNode protectedNode = JSON_NODE_FACTORY.objectNode();
    protectedNode.set("jwk", OBJECT_MAPPER.readTree(newPublicJwk(getPublicTestKey()).toJson()));
    final ObjectNode payloadNode = JSON_NODE_FACTORY.objectNode();
    final String jsonInput = compactSerializationOf(protectedNode, payloadNode, getPrivateTestKey());
    try {
      fail("Expecting InvalidAcmeMessageException, but got: " + new MyAcmeRequest(new NoJOSEjwk()).convert(jsonInput));
    } catch (InvalidAcmeMessageException expected) {
      expected.printStackTrace();
    }
  }

  @Test
  public void conversionSucceedsProvidingPublicKey() throws Exception {
    final Nonce knownNonce = new Nonce();
    given(nonceDAOMock.checkAndForget(knownNonce)).willReturn(true);
    final ObjectNode protectedNode = JSON_NODE_FACTORY.objectNode();
    protectedNode.set("jwk", OBJECT_MAPPER.readTree(newPublicJwk(getPublicTestKey()).toJson()));
    protectedNode.put("nonce", knownNonce.getValue());
    protectedNode.put("url", DUMMY_URL_VALUE);
    final ObjectNode payloadNode = JSON_NODE_FACTORY.objectNode();
    final String jsonInput = compactSerializationOf(protectedNode, payloadNode, getPrivateTestKey());
    {
      final Context<Pair<String, String>> context = new MyAcmeRequest(new JOSEjwk(), new NoJOSEkid()).convert(jsonInput);
      then(context.has(JOSEkid.INSTANCE).isPresent()).as("JOSE kid available").isFalse();
      then(context.getKeyToVerifyJws()).as("JOSE jwk available").isNotNull();
    }
  }

  @Test
  public void conversionOfExpectedPayload() throws Exception {
    final Nonce knownNonce = new Nonce();
    given(nonceDAOMock.checkAndForget(knownNonce)).willReturn(true);
    final URL kidValue = new URL("http://key.id");
    given(accountDAOMock.getPublicKeyWith(kidValue)).willReturn(of(getPublicTestKey()));
    final ObjectNode protectedNode = JSON_NODE_FACTORY.objectNode();
    protectedNode.put("kid", kidValue.toExternalForm());
    protectedNode.put("nonce", knownNonce.getValue());
    protectedNode.put("url", DUMMY_URL_VALUE);
    final ObjectNode payloadNode = JSON_NODE_FACTORY.objectNode();
    payloadNode.put("field", "value");
    final String jsonInput = compactSerializationOf(protectedNode, payloadNode, getPrivateTestKey());
    {
      final Context<Pair<String, String>> context = new MyAcmeRequest().convert(jsonInput);
      then(context.has(JOSEkid.INSTANCE).isPresent()).as("JOSE kid available").isTrue();
      then(context.getKeyToVerifyJws()).as("JOSE jwk available").isEqualTo(getPublicTestKey());
      final Pair<String, String> payload = context.getPayload();
      then(payload).as("Payload of request").isNotNull();
      then(payload.getKey()).as("Key of payload").isEqualTo("field");
      then(payload.getValue()).as("Value of payload").isEqualTo("value");
    }
  }

  @Test
  public void kidAddressingUnknownAccount() throws Exception {
    final Nonce knownNonce = new Nonce();
    final URL kidValue = new URL("http://key.id");
    final ObjectNode protectedNode = JSON_NODE_FACTORY.objectNode();
    protectedNode.put("kid", kidValue.toExternalForm());
    protectedNode.put("nonce", knownNonce.getValue());
    final ObjectNode payloadNode = JSON_NODE_FACTORY.objectNode();
    payloadNode.put("field", "value");
    final String jsonInput = compactSerializationOf(protectedNode, payloadNode, getPrivateTestKey());
    try {
      fail("Expecting InvalidAcmeMessageException but got: " + new MyAcmeRequest().convert(jsonInput));
    } catch (InvalidAcmeMessageException expected) {
      // expected
    }
  }

  @Test
  public void plainJsonJwsConversion() {
    final ObjectNode jwsRootNode = JSON_NODE_FACTORY.objectNode();
    final ObjectNode protectedNode = jwsRootNode.putObject("protected");
    final String kidValue = "http://key.id";
    protectedNode.put("kid", kidValue);
    final ObjectNode payloadNode = jwsRootNode.putObject("payload");
    payloadNode.put("field", "value");
    jwsRootNode.put("signature", "fooSignature");
    final String jsonInput = jwsRootNode.toString();
    {
      final JwtContext jwtContext = PlainJsonJwsSerializationToJwtContextConverter.INSTANCE.convert(jsonInput);
      then(jwtContext).as("JWT context").isNotNull();
      final JwtClaims jwtClaims = jwtContext.getJwtClaims();
      then(jwtClaims.toJson()).as("JWT claims").isEqualTo("{\"field\":\"value\"}");
      final List<JsonWebStructure> joseObjects = jwtContext.getJoseObjects();
      then(joseObjects.size()).as("Amount of JOSE objects").isEqualTo(1);
      final JsonWebStructure firstJoseObject = joseObjects.get(0);
      then(firstJoseObject).as("1st JOSE object").isInstanceOf(JsonWebSignature.class);
      final JsonWebSignature jsonWebSignature = (JsonWebSignature) firstJoseObject;
      then(jsonWebSignature.getKeyIdHeaderValue()).as("kid header").isEqualTo(kidValue);
    }
  }

  @Test
  public void flattenedJwsJsonConversion() {
    final ObjectNode jwsRootNode = JSON_NODE_FACTORY.objectNode();
    final String kidValue = "http://key.id";
    jwsRootNode.put("protected", encodeUtf8ByteRepresentation("{\"kid\":\"" + kidValue + "\"}"));
    jwsRootNode.put("payload", encodeUtf8ByteRepresentation("{\"field\":\"value\"}"));
    jwsRootNode.put("signature", encodeUtf8ByteRepresentation("fooSignature"));
    final String jsonInput = jwsRootNode.toString();
    {
      final JwtContext jwtContext = AcmeRequest.FlattenedJwsJsonSerializationToJwtContextConverter.INSTANCE.convert(jsonInput);
      then(jwtContext).as("JWT context").isNotNull();
      final JwtClaims jwtClaims = jwtContext.getJwtClaims();
      then(jwtClaims.toJson()).as("JWT claims").isEqualTo("{\"field\":\"value\"}");
      final List<JsonWebStructure> joseObjects = jwtContext.getJoseObjects();
      then(joseObjects.size()).as("Amount of JOSE objects").isEqualTo(1);
      final JsonWebStructure firstJoseObject = joseObjects.get(0);
      then(firstJoseObject).as("1st JOSE object").isInstanceOf(JsonWebSignature.class);
      final JsonWebSignature jsonWebSignature = (JsonWebSignature) firstJoseObject;
      then(jsonWebSignature.getKeyIdHeaderValue()).as("kid header").isEqualTo(kidValue);
    }
  }

  private String compactSerializationOf(final ObjectNode protectedNode, final ObjectNode payloadNode, final PrivateKey
          signingKey) throws JsonProcessingException, GeneralSecurityException, UnsupportedEncodingException {
    final Signature signature = Signature.getInstance("SHA256withRSA");
    signature.initSign(signingKey);
    protectedNode.put("alg", RSA_USING_SHA256);
    final String dataToSign = encodeUtf8ByteRepresentation(OBJECT_MAPPER.writeValueAsString(protectedNode))
            + "." + encodeUtf8ByteRepresentation(OBJECT_MAPPER.writeValueAsString(payloadNode));
    signature.update(dataToSign.getBytes("UTF-8"));
    final String signatureData = Base64Url.encode(signature.sign());
    return dataToSign + "." + signatureData;
  }

  private final class MyAcmeRequest extends AcmeRequest<Pair<String, String>> {

    private MyAcmeRequest(final JoseHeaderPredicate... joseHeaderPredicates) {
      super(accountDAOMock, nonceDAOMock, joseHeaderPredicates);
    }

    private MyAcmeRequest() {
      this(NoJOSEjwk.INSTANCE, JOSEkid.INSTANCE);
    }

    @Override
    protected Pair<String, String> parsePayloadFrom(final JwtClaims jwtClaims) throws InvalidAcmeMessageException {
      try {
        return new ImmutablePair<>("field", jwtClaims.getStringClaimValue("field"));
      } catch (MalformedClaimException e) {
        throw new InvalidAcmeMessageException("Malformed claim 'field'", jwtClaims.toJson(), e);
      }
    }

  }

}