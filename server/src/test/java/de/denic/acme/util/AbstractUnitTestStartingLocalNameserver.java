/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.util;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.xbill.DNS.tools.jnamed;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import static java.io.File.pathSeparator;
import static java.net.InetAddress.getLocalHost;
import static java.util.Arrays.stream;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.apache.commons.lang3.StringUtils.join;
import static org.apache.commons.lang3.SystemUtils.JAVA_CLASS_PATH;
import static org.apache.commons.lang3.SystemUtils.USER_DIR;
import static org.junit.Assert.assertTrue;

public abstract class AbstractUnitTestStartingLocalNameserver {

  protected static final String NAME_OF_LOCAL_HOST;
  private static Process jnamedProcess;
  private static InputStreamReadingRunnable jnamedSysoutReader;

  static {
    try {
      NAME_OF_LOCAL_HOST = getLocalHost().getCanonicalHostName();
    } catch (final UnknownHostException e) {
      throw new ExceptionInInitializerError(e);
    }
  }
  // private static File jnamedCacheFile;

  /**
   * Starts up the jnamed name server in a separate process.
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    // createJNamedCacheFile();
    startJNamedProcess();
    startSysOutReadingThread();
    Thread.sleep(SECONDS.toMillis(1L)); // Waiting for jnamed process startup
    assertTrue("PRECONDITION: jnamed process not alive anymore!", jnamedProcess.isAlive());
  }

  private static void startSysOutReadingThread() {
    jnamedSysoutReader = new InputStreamReadingRunnable(jnamedProcess.getInputStream());
    final Thread systemOutReadingThread = new Thread(jnamedSysoutReader, "jnamed sys.out reader");
    systemOutReadingThread.setDaemon(true);
    systemOutReadingThread.start();
  }

  /**
   *
   */
  private static void startJNamedProcess() throws IOException {
    final Optional<Path> dnsjavaLibInClassPath = stream(JAVA_CLASS_PATH.split(pathSeparator))
            .map(pathName -> Paths.get(pathName)).filter(path -> {
              final String fileName = path.getFileName().toString();
              return fileName.startsWith("dnsjava-") && fileName.endsWith(".jar");
            })
            .findFirst();
    assertTrue("PRECONDITION: dnsjava JAR file available in class path", dnsjavaLibInClassPath.isPresent());
    final Path dnsjavaLibFile = dnsjavaLibInClassPath.get();
    final Optional<Path> slf4jApiLibInClassPath = stream(JAVA_CLASS_PATH.split(pathSeparator))
            .map(pathName -> Paths.get(pathName)).filter(path -> {
              final String fileName = path.getFileName().toString();
              return fileName.startsWith("slf4j-api-") && fileName.endsWith(".jar");
            })
            .findFirst();
    assertTrue("PRECONDITION: slf4j-api JAR file available in class path", slf4jApiLibInClassPath.isPresent());
    final Path slf4jApiLibFile = slf4jApiLibInClassPath.get();
    final File directory;
    if (USER_DIR.endsWith("server")) {
      directory = new File(USER_DIR);
    } else {
      directory = new File(USER_DIR, "server");
    }
    final ProcessBuilder processBuilder = new ProcessBuilder("java",
            "-cp", dnsjavaLibFile + pathSeparator + slf4jApiLibFile,
            jnamed.class.getCanonicalName(),
            "./src/test/resources/jnamed/jnamed.conf")
            .redirectErrorStream(true)
            .directory(directory);
    System.out.println("!!! Launching jnamed process: <" + processBuilder.directory() + ">$ " + join(processBuilder.command(), ' '));
    jnamedProcess = processBuilder.start();
  }

  // private static void createJNamedCacheFile() throws IOException {
  // jnamedCacheFile = new File("./build/tmp/jnamed/cache.db").getCanonicalFile();
  // createDirectories(jnamedCacheFile.toPath());
  // if (!jnamedCacheFile.isFile()) {
  // assertTrue("PRECONDITION: Create jnamed cache file", jnamedCacheFile.createNewFile());
  // }
  // System.out.println("!!! Created jnamed cache file: " + jnamedCacheFile);
  // }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    if (jnamedProcess != null) {
      jnamedProcess.destroy();
      jnamedProcess.waitFor(3L, SECONDS);
      System.out.println("!!! Stopping jnamed process yields exit code " + jnamedProcess.exitValue());
    }
    // if (jnamedCacheFile != null) {
    // jnamedCacheFile.deleteOnExit();
    // System.out.println("!!! Deleting jnamed cache file " + jnamedCacheFile + " on exit");
    // }
  }

}
