/*^
  ===========================================================================
  ACME server
  ===========================================================================
  Copyright (C) 2017-2019 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.acme.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import static org.apache.commons.io.IOUtils.toByteArray;
import static org.apache.commons.lang3.SystemUtils.getUserDir;

public final class TestKeyUtils {

  public static final File PROJECT_BASE_DIR = new File(getUserDir(), ("server".equals(getUserDir().getName()) ? "." : "server"));
  public static final File TEST_RESOURCES_FOLDER = new File(PROJECT_BASE_DIR, "src/test/resources/");
  public static final File PRIVATE_KEY_FILE = new File(TEST_RESOURCES_FOLDER
          , "acme-test-client-rsa-key.priv.PKCS8");
  public static final File PUBLIC_KEY_FILE = new File(TEST_RESOURCES_FOLDER
          , "acme-test-client-rsa-key.pub.X509");
  public static final KeyFactory RSA_KEY_FACTORY;

  static {
    try {
      RSA_KEY_FACTORY = KeyFactory.getInstance("RSA");
    } catch (NoSuchAlgorithmException e) {
      throw new ExceptionInInitializerError(e);
    }
  }

  public static PublicKey getPublicTestKey() throws IOException, InvalidKeySpecException {
    System.out.println("Loading public key from " + PUBLIC_KEY_FILE);
    try (final InputStream keyFileInput = new FileInputStream(PUBLIC_KEY_FILE)) {
      return RSA_KEY_FACTORY.generatePublic(new X509EncodedKeySpec(toByteArray(keyFileInput)));
    }
  }

  public static PrivateKey getPrivateTestKey() throws IOException, InvalidKeySpecException {
    System.out.println("Loading private key from " + PRIVATE_KEY_FILE);
    try (final InputStream keyFileInput = new FileInputStream(PRIVATE_KEY_FILE)) {
      return RSA_KEY_FACTORY.generatePrivate(new PKCS8EncodedKeySpec(toByteArray(keyFileInput)));
    }
  }

}
