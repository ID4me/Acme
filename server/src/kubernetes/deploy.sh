#!/bin/bash
set -e

_fullscriptpath="$(readlink -f ${BASH_SOURCE[0]})"
BASEDIR="$(dirname $_fullscriptpath)"

DOCKERREGISTRY=${DOCKER_REGISTRY}
DOCKERREGISTRYPATH=test
IMAGENAME=acme-server

NAMESPACE=$1
KUBECONFIG="$KUBECONFIG --namespace=${NAMESPACE}"
VERSION=$2

CONFIGMAPFILE=acme-server_configmap_${NAMESPACE}-ns.yaml
if [ -f ${CONFIGMAPFILE} ]; then
	echo "Applying config map file: ${CONFIGMAPFILE}"
	$KUBECONFIG apply --filename=${CONFIGMAPFILE}
fi

sed --in-place "s#__DOCKER_REGISTRY__#${DOCKERREGISTRY}#" acme-server-dc.yaml
sed --in-place "s#__IMAGE_VERSION__#${VERSION}#" acme-server-dc.yaml
$KUBECONFIG apply --filename=acme-server-dc.yaml --record
$KUBECONFIG rollout status deployment/acme-server --watch
