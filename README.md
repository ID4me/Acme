# Acme-Server

## Build

```bash
git clone https://gitlab.com/ID4me/Acme.git
cd Acme
export VERSION=$(date --utc +%y%m%d.%H%M --date="@$(git show --no-patch --format=%ct)")
./gradlew --no-daemon -Pversion=${VERSION} clean build

```

## Run

```bash
export dns_resolver_host=1.1.1.1
java -jar server/build/libs/server-${VERSION}.jar
```

