#!groovy
pipeline {
	agent any
	options {
		buildDiscarder(logRotator(numToKeepStr: '24'))
		disableConcurrentBuilds()
	}
	environment {
		VERSION = sh (script: 'date --utc +%y%m%d.%H%M --date="@$(git show --no-patch --format=%ct)"', returnStdout: true).trim()
		//DOCKER_REGISTRY = 'kube-registry.freedom-id.de'
		DOCKER_REGISTRY_PATH = 'test'
		KUBECONFIG = '/var/jenkins_home/kubectl --kubeconfig=/var/jenkins_home/secrets/jenkins-kubectl.conf'
	}
	triggers {
		pollSCM('TZ=Europe/Berlin\n' + 'H/5 8-18 * * 1-5')
	}
	stages {

		stage('Checkout') {
			steps {
                checkout([$class: 'GitSCM',
                		branches: [[name: '*/master']],
						doGenerateSubmoduleConfigurations: false,
						extensions: [],
						submoduleCfg: [],
						userRemoteConfigs: [[credentialsId: 'domainid-bot-ssh-key', url: 'git@gitlab.com:ID4me/Acme.git']]])
			}
		}

		stage('Build') {
			steps {
				timestamps {
					timeout(time: 7, unit: 'MINUTES') {
						sh "./gradlew -Pversion=${VERSION} --no-daemon clean build javadoc projectReport"
					}
				}
			}
			post {
				always {
					junit '*/build/test-results/test/*.xml'
					jacoco classPattern: '*/build/classes/java',
							execPattern: '*/build/jacoco/*.exec',
							sourcePattern: '*/src/*/java'
					findbugs canComputeNew: false,
							defaultEncoding: 'ISO-8859-1',
							healthy: '0',
							pattern: '*/build/reports/spotbugs/*.xml',
							unHealthy: '3'
					pmd canComputeNew: false,
							defaultEncoding: 'ISO-8859-1',
							healthy: '0',
							pattern: '*/build/reports/pmd/*.xml',
							unHealthy: '3'
				}
			}
		}

		stage('Archiving artifacts and publishing reports') {
			steps {
				parallel (
					'Archive artifacts': {
						archiveArtifacts artifacts: '*/build/libs/*.jar', onlyIfSuccessful: true
					},
					'Server JavaDoc': {
						publishHTML([allowMissing: true,
                                     alwaysLinkToLastBuild: false,
                                     keepAll: false,
                                     reportDir: 'server/build/docs/javadoc',
                                     reportFiles: 'index.html',
                                     reportName: 'Server JavaDoc',
                                     reportTitles: 'Server JavaDoc'])
					/*
					},
					'Build profile': {
						publishHTML([allowMissing: true,
                                     alwaysLinkToLastBuild: false,
                                     keepAll: false,
                                     reportDir: 'build/reports/profile',
                                     reportFiles: 'profile-*.html',
                                     reportName: 'Gradle Build Profile',
                                     reportTitles: 'Gradle Build Profile'])
					*/
					}
				)
			}
		}

		stage('Build and push Docker image') {
			steps {
				dir('./server') {
					timestamps {
						timeout(time: 90, unit: 'SECONDS') {
							sh "./docker-build.sh \"${DOCKER_REGISTRY}\" \"${DOCKER_REGISTRY_PATH}\" \"${VERSION}\""
						}
					}
				}
			}
		}

		stage('Cluster deployment to staging namespace') {
			steps {
				dir('./server/src/kubernetes') {
					timestamps {
						timeout(time: 3, unit: 'MINUTES') {
							sh "./deploy.sh staging ${VERSION}"
						}
					}
				}
			}
		}

		stage('Integration Tests') {
			steps {
				timestamps {
					timeout(time: 120, unit: 'SECONDS') {
						sh "./gradlew -Pversion=${VERSION} --no-daemon intTest"
					}
				}
			}
			post {
				always {
					junit '*/build/test-results/intTest/*.xml'
				}
			}
		}

		stage('Cluster deployment to default namespace') {
			steps {
				dir('./server/src/kubernetes') {
					timestamps {
						timeout(time: 3, unit: 'MINUTES') {
							sh "./deploy.sh default ${VERSION}"
						}
					}
				}
			}
		}

	}
}
